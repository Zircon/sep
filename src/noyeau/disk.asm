		ctnu:

			mov dx, 0x1f1
			mov al, 0x00
			out dx, al

			mov dx, 0x1f2
			mov al, 0x00
			out dx, al
		
			mov dx, 0x1f3
			mov al, 0x3f
			out dx, al
		
			mov dx, 0x1f4
			mov al, 0x00
			out dx, al
		
			mov dx, 0x1f5
			mov al, 0x00
			out dx, al
		
			mov dx, 0x1f6
			mov al, 0xe0
			out dx, al
		
			mov dx, 0x1f7
			mov al, 0x20
			out dx, al
			
			; This can acess 128 petabits in 48bits mode
			
			AfficherTexte(texte)
			
			call __read_stats__

			AfficherTexte(texte)

			xor ecx, ecx
			__read_data__ :
				mov dx, 0x01f0
				in ax, dx
				
				AfficherCaractere(al)
				AfficherCaractere(ah)
				
				mov [ecx*2+0x100000], ax
				inc ecx
				
				cmp ecx, 256
				jb __read_data__
			
			
					jmp nextstep

			ret
			
			[section .bss]
				__IO_DISK__
					.1: dw 0x01f0
					.2: dw 0x03f4
					.3: dw 0x0170
					.4: dw 0x0374
			
				Part1: resb 16
			
			[section .text]
			
		nextstep:

		
		%define ATA_REG_DATA       0x00
		%define ATA_REG_ERROR      0x01
		%define ATA_REG_FEATURES   0x01
		%define ATA_REG_SECCOUNT0  0x02
		%define ATA_REG_LBA0       0x03
		%define ATA_REG_LBA1       0x04
		%define ATA_REG_LBA2       0x05
		%define ATA_REG_HDDEVSEL   0x06
		%define ATA_REG_COMMAND    0x07
		%define ATA_REG_STATUS     0x07
		%define ATA_REG_SECCOUNT1  0x08
		%define ATA_REG_LBA3       0x09
		%define ATA_REG_LBA4       0x0A
		%define ATA_REG_LBA5       0x0B
		%define ATA_REG_CONTROL    0x0C
		%define ATA_REG_ALTSTATUS  0x0C
		%define ATA_REG_DEVADDRESS 0x0D
		
		;------------------------------
		; AX : Disk id
		;------------------------------
		__read_status_reg__:
			mov dx,  ax
			xor eax, eax
			in  al,  dx
			
			ret
		
		
		
		
		
		
		
		
		
		__read_data__:
			cmp ah, 0x00
			jz .__read_sector__
			
			cmp ah, 0x01
			jz .__read_sectors__
			
			cmp ah, 0x02
			jz .__read_bytes__
			
			; Error no command found.
			
			ret
			
			;-----------------------------------------------
			; Data to push on the stack in this order:
			;  - info struct pointer | 32 bits
			;     + disk number      | 8  bits
			;     + sector count     | 16 bits
			;     + sector number    | 48 bits
			;     + buffer pointer   | 32 bits
			;-----------------------------------------------
			.__read_sector__:
				pop ecx
			
				; Get the i/o port of the disk.
				xor edx, edx
				mov dl, [ecx]
				shr dl
				add edx, __IO_DISK__
				mov dx, [edx]
				
				; seting the ide controler.
				mov eax, 0x00000000
				inc dx
				out dx, al
				
				mov al, [ecx + 2]
				inc dx
				out dx, al
				
				mov al, [ecx + 8]
				inc dx
				out dx, al

				mov al, [ecx + 7]
				inc dx
				out dx, al
				
				mov al, [ecx + 6]
				inc dx
				out dx, al
				
				
				
				mov al, ch
				inc dx
				out dx, al
			
				
		
		
		