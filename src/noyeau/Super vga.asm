%ifidn ___copmilataion_system_base___, unix
	%include "Noyeau/Terminal/Terminal.inc"
	%include "Noyeau/Terminal/Terminal.s"
%elifidn ___copmilataion_system_base___, win32
	%include "E:\Projet Sep\Noyeau\Terminal\Terminal.inc"
	%include "E:\Projet Sep\Noyeau\Terminal\Terminal.s"
%else
	%error "Compilation impossible, le systeme qui permet de compiler n'est pas defini."
%endif

CONTROLEUR_GRAPHIQUE  : db "Contr�leur Graphique ", 0x00
	CIRRUS_LOGIC  	  : db "Cirrus Logic ", "CL-GD54", 0x00
		CL_GD5420     : db "20.", 0x00
		CL_GD5420_R1  : db "20 R�vision 1.", 0x00
		CL_GD5422     : db "22.", 0x00
		CL_GD5424     : db "24.", 0x00
		CL_GD5426     : db "26.", 0x00
		CL_GD5428     : db "28.", 0x00
		CL_GD5429     : db "29.", 0x00
		CL_GD5430_40  : db "30/40.", 0x00
		CL_GD5434_R4  : db "34 R�vision 4.", 0x00
		CL_GD5434_R8  : db "34 R�vision 8.", 0x00
		CL_GD5436     : db "36.", 0x00
		CL_GD5446     : db "46.", 0x00
		CL_GD5462     : db "62.", 0x00
		CL_GD5464     : db "64.", 0x00
		CL_GD5464_RBD : db "64 R�vision BD.", 0x00
		CL_GD5465     : db "65.", 0x00
		CL_GD5480     : db "80.", 0x00
		CL_GD54XX     : db "XX, cette version du contr�leur Super VGA de Cirrus Logic est inconu "
						db "du syst�me. Un pilote g�n�rique sera donc utilis�.", CR, 0x00
	NO_CIRRUS_LOGIC   : db "Aucun contr�leur Cirrus Logic fut trouver.", CR, 0x00

AfficherTexte ( CONTROLEUR_GRAPHIQUE )

; Super vga de Cirrus Logic ;
Cirrus_Logic_Test_Debut :
	mov AL, 0x27
	mov DX, 0x03D4
	out DX, AL
	
	mov DX, 0x03D5
	in  AL, DX

	cmp AL, 0x00
	cmovz bx, 0x0000
	jz NEAR Cirrus_Logic_Test_Fin

	AfficherTexte ( CIRRUS_LOGIC )

	cmp AL, 0x8A
	jz NEAR CL_GD5420_A
	
	cmp AL, 0x8B
	jz NEAR CL_GD5420_R1_A

	cmp AL, 0x8C
	jz NEAR CL_GD5422_A

	cmp AL, 0x94
	jz NEAR CL_GD5424_A
	
	cmp AL, 0x90
	jz NEAR CL_GD5426_A

	cmp AL, 0x98
	jz NEAR CL_GD5428_A
	
	cmp AL, 0x9C
	jz NEAR CL_GD5429_A

	cmp AL, 0xA0
	jz NEAR CL_GD5430_40_A
	
	cmp AL, 0xA4
	jz NEAR CL_GD5434_R4_A

	cmp AL, 0xA8
	jz NEAR CL_GD5434_R8_A
	
	cmp AL, 0xAC
	jz NEAR CL_GD5436_A
	
	cmp AL, 0xB8
	jz NEAR CL_GD5446_A
	
	cmp AL, 0xD0
	jz NEAR CL_GD5462_A

	cmp AL, 0xD4
	jz NEAR CL_GD5464_A
	
	cmp AL, 0xD5
	jz NEAR CL_GD5464_RBD_A
	
	cmp AL, 0xD6
	jz NEAR CL_GD5465_A
	
	cmp AL, 0xDC
	jz NEAR CL_GD5480_A
	
	jmp CL_GD54XX_A
		
	CL_GD5420_A     :
		AfficherTexte ( CL_GD5420 )
		ret
	CL_GD5420_R1_A  :
		AfficherTexte ( CL_GD5420_R1 )
		ret
	CL_GD5422_A     :
		AfficherTexte ( CL_GD5422 )
		ret
	CL_GD5424_A     :
		AfficherTexte ( CL_GD5424 )
		ret
	CL_GD5426_A     :
		AfficherTexte ( CL_GD5426 )
		ret
	CL_GD5428_A     :
		AfficherTexte ( CL_GD5428 )
		ret
	CL_GD5429_A     :
		AfficherTexte ( CL_GD5429 )
		ret
	CL_GD5430_40_A  :
		AfficherTexte ( CL_GD5430_40 )
		ret
	CL_GD5434_R4_A  :
		AfficherTexte ( CL_GD5434_R4 )
		ret
	CL_GD5434_R8_A  :
		AfficherTexte ( CL_GD5434_R8 )
		ret
	CL_GD5436_A     :
		AfficherTexte ( CL_GD5436 )
		ret
	CL_GD5446_A     :
		AfficherTexte ( CL_GD5446 )
		ret
	CL_GD5462_A     :
		AfficherTexte ( CL_GD5462 )
		ret
	CL_GD5464_A     :
		AfficherTexte ( CL_GD5464 )
		ret
	CL_GD5464_RBD_A :
		AfficherTexte ( CL_GD5464_RBD )
		ret
	CL_GD5465_A     :
		AfficherTexte ( CL_GD5465 )
		ret
	CL_GD5480_A     :
		AfficherTexte ( CL_GD5480 )
		ret

	CL_GD54XX_A     :
		AfficherTexte ( CL_GD54XX )

	Cirrus_Logic_Test_Fin :
		AfficherTexte ( NO_CIRRUS_LOGIC )
		ret
