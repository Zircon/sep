[BITS 32]
[ORG 0x1000]

jmp main

%define ___copmilataion_system_base___ unix

%ifidn ___copmilataion_system_base___, unix
	%include "inc/noyeau/terminal/terminal.inc"
	%include "src/noyeau/terminal/terminal.asm"
	%include "inc/noyeau/descriptors/gdt.inc"
	%include "src/noyeau/descriptors/gdt.asm"
	%include "src/noyeau/descriptors/idt.asm"
	%include "src/noyeau/cpu.asm"

%elifidn ___copmilataion_system_base___, win32
	%include "E:\Documents\Documentations\Sep\inc\noyeau\terminal\Terminal.inc"
	%include "E:\Documents\Documentations\Sep\src\noyeau\terminal\Terminal.asm"
	%include "E:\Documents\Documentations\Sep\src\noyeau\Super vga.asm"
	%include "E:\Documents\Documentations\Sep\inc\noyeau\descriptors\DTs.inc"
	%include "E:\Documents\Documentations\Sep\src\noyeau\descriptors\DTs.asm"
%else
	%error "Compilation impossible, le systeme qui permet de compiler n'est pas defini."
%endif

picture : db "````````````````````````````````````````````````C)C)!!!!>X>X````"
		  db "````6666!!!!KKKK````````````````````````````````````````````````"
		  db "````````````````````````````````````````````````C)C)!!!!>X>X````"
		  db "````6666!!!!KKKK````````````````````````````````````````````````"
		  db "````````````````````````````````````````````````C)C)!!!!>X>X````"
		  db "````6666!!!!KKKK````````````````````````````````````````````````"
		  db "````````````````````````````````````````````````C)C)!!!!>X>X````"
		  db "````6666!!!!KKKK````````````````````````````````````````````````"
		  db "````````G:G:2%2%!!!!!!!!!!!!%2%2>X>X````````````C)C)!!!!>X>X````"
		  db "````6666!!!!KKKK````````````X>X>6666!!!!!!!!!!!!6666T-T-````````"
		  db "````C)C)!!!!!!!!!!!!!!!!!!!!!!!!!!!!6666````````C)C)!!!!>X>X````"
		  db "````6666!!!!KKKK````````KKKK!!!!!!!!!!!!!!!!!!!!!!!!!!!!KKKK````"
		  db "X>X>!!!!%2%2KKKK````````````:G:G!!!!!!!!\O\O````C)C)!!!!>X>X````"
		  db "````6666!!!!KKKK````\O\O)C)C!!!!2%2%X>X>````\O\O2%2%!!!!%2%2\O\O"
		  db "````````````````````````````\O\O!!!!!!!!\O\O````C)C)!!!!>X>X````"
		  db "````6666!!!!KKKK````G:G:!!!!-T-T````````````````````-T-T!!!!C)C)"
		  db "````````````\O\O>X>X>X>X>X>X%2%2!!!!!!!!\O\O````C)C)!!!!>X>X````"
		  db "````6666!!!!KKKK````>X>X!!!!C)C)````````````````````C)C)!!!!6666"
		  db "````KKKK)C)C!!!!!!!!!!!!!!!!!!!!!!!!!!!!\O\O````C)C)!!!!>X>X````"
		  db "````6666!!!!KKKK````6666!!!!C)C)````````````````````KKKK!!!!6666"
		  db "X>X>%2%2!!!!2%2%>X>X>X>XC)C)\O\O!!!!!!!!\O\O````C)C)!!!!>X>X````"
		  db "````6666!!!!KKKK````>X>X!!!!C)C)````````````````````C)C)!!!!6666"
		  db "C)C)!!!!:G:G````````````````KKKK!!!!!!!!X>X>````C)C)!!!!>X>X````"
		  db "````6666!!!!KKKK````G:G:!!!!-T-T````````````````````-T-T!!!!C)C)"
		  db "C)C)!!!!-T-TX>X>````````C)C)!!!!!!!!!!!!T-T-````C)C)!!!!>X>X````"
		  db "````6666!!!!KKKK````\O\O%2%2!!!!2%2%X>X>````\O\O2%2%!!!!%2%2\O\O"
		  db "\O\O%2%2!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!O\O\````C)C)!!!!>X>X````"
		  db "````6666!!!!KKKK````````KKKK!!!!!!!!!!!!!!!!!!!!!!!!!!!!G:G:````"
		  db "````T-T-2%2%!!!!!!!!!!!!6666G:G:6666!!!!C)C)````C)C)!!!!>X>X````"
		  db "````6666!!!!KKKK````````````T-T-6666!!!!!!!!!!!!6666T-T-````````"
		  db "````````````````````````````````````````````````````````````````"
		  db "````````````````````````````````````````````````````````````````"
		  db "````_P0O].X1\^P.]_8;`@LY`````````````@LY]_8;\^P.\^P.^OTE````````"
		  db "].X1^OTE````````````````].X1_/\I````````^/@?\^P.\^P.]_8;`PX]````"
		  db "\O<DL).RL).RL).RL).RNZ3%````````_@@WNZ3%L).RL).RL).RL).RW-7^````"
		  db "P:S.QK38````````````[>\:L).RY^<1````QK38L).RL).RL).RL).RS+SA````"
		  db "LYR`?5!K?5!K?5!K?5!K?5!KS</K````Q+;=?5!K?5!K?5!K?5!K?5!KCVF'````"
		  db "LYR`AEUY````````````S</K?5!K\?<DW]T'?5!K?5!K?5!K?5!K?5!K?5!K\?<D"
		  db "CFB&A%IV\/0A````TLGR<3Y7OZW3````F':5>TQGYN81````YN81A%IV<3Y7^@(P"
		  db "R;OC<3Y7\/0A````````JY*TA%IV````OZW3<3Y7OZW3````^@(PF':5<3Y7TLGR"
		  db 'E7".GGV<````````````````````````@E5PIXJK````````````L)BZ>4AA\?4B'
		  db "WML%>4AAU<WV````````GGV<GGV<````IXJKBV-_````````````S+_G>4AAS+_G"
		  db "H7^>B5MURKOA````````````````````````````````````````PJ_4B5MUZ^P7"
		  db "````D6>#RKOA````````D6>#RKOA````````````````````````VM/\B5MURKOA"
		  db "Q[/7GW61GW61GW61U,?N````````````````````````[_$=SKWCIG^=GW61[_$="
		  db "````LY2TP*C+````[_$=GW61XMT&````````````````_08TSKWCLY2TGW61U,?N"
		  db "]/<CMY.OMY.OMY.OMY.OQJS,_P@V````````X-7\MY.OMY.OMY.OMY.OMY.O]/<C"
		  db '````U<7IP:3"````Y=X&MY.O^?\M````````ZN<0O)NYMY.OMY.OMY.OMY.OX-7'
		  db "\````[NH2S[#-S[#-S[#-S[#-Y]T#````Z^0*S[#-S[#-W<GLY]T#Y-;[S[#-^?TI"
		  db "````[NH2S[#-`0HXY]T#UKW<````````^?TIS[#-S[#-UKW<Y]T#Z^0*S[#-Z^0*"
		  db "`````````@LZ\NL1Y,KHY,KHZ-/T````Z-/TZMCZ````````````\NL1Y,KH_@(N"
		  db "````^_XIY,KH^_XI[-S_[-S_````````\.<+YL_N_`<T````````^?DCY,KH]>`7"
		  db "````````````````````^.@*]N$!`@DW]=W\_/D@````````````^_(7]=W\`08R"
		  db "````````]N$!_/D@]N$!_/D@````````]^4%^N\3````````````_?PE]=W\_/D@"
		  db "_NL*`/@=`````````````/@=_NL*`P@T_N@&`?TE``````````T\_NL*_N@&`P@T"
		  db "````````__,5__,5_N@&`@,M````````_NL*`/49````````````_^`2_N@&`?TE"
		  db "_.<$_./``/`I`````PHW_.<$_.<$`PHW^^#[__H@`````PT[_>X-^^#[^^#[`@<R"
		  db "````````_O07_./`^^#[`PHW````````_.<$_>X-````````_O07^^#[^^#[__H@"
		  db '].<)Y\+8Y\+8Y\+8Y\+8Y\+8\N("````Z<??Y\+8Y\+8Y\+8Y\+8Y\+8Y\+8_/LD'
		  db "````````^/$6Y\+8Z\SF````````````[M?TY\+8Y\+8Y\+8Y\+8Y\+8Y\+8]NP/"
		  db '^_`LSJ"QRIBGRIBGRIBGSJ"Q^_`L````Y=#NRIBGRIBGRIBGRIBGW<#:RIBG[-`#'
		  db "````````^/@ARIBGV;C0````````````\.@-RIBGRIBGRIBGRIBGW<#:RIBGX<CD"
		  db "````YMK^N(64K7!YN(64YMK^````````_@4SQ)JNK7!YN(64YMK^^/LEK7!YV\7C"
		  db "````````````LWN&V\7C````````````````S[#)K7!YK7!YV\7C````N(64Q)JN"
		  
		  
		  db "````````````````````````````````````````````````"
		  db "````````````\K[YC)Q!!Q!!Q!!Q!!Q!!X>X]T-`````````"
		  db "`````````V26Q!!Q!!Q!!Q!!Q!!Q!!Q!!Q!!Q!!ZG:``````"
		  db "``````YC)Q!!Q!!X:W````````````X>XQ!!Q!!R)2``````"
		  db "``````R%2Q!!W:G``````````````````W:GQ!!Q!!^X>```"
		  db "`````````````````````````````````YG*Q!!Q!!\O\```"
		  db "````````````````````````ZG:YC)W:GQ!!Q!!Q!!\S]```"
		  db "`````````_\OYC)R%2Q!!Q!!Q!!Q!!Q!!Q!!Q!!Q!!\O\```"
		  db "``````]T-R%2Q!!Q!!Q!!Q!!R)2YC)YC)YC)Q!!Q!!\O\```"
		  db "``````S)CQ!!Q!!YG*_\O````````````YC)Q!!Q!!\O\```"
		  db "```[KKQ!!Q!!ZG:``````````````````X:WQ!!Q!!\O\```"
		  db "```ZG:Q!!Q!!^X>``````````````````S%BQ!!Q!!\O\```"
		  db "```[KKQ!!Q!!\O\```````````````YC)Q!!Q!!Q!!\O\```"
		  db "```_\OR)3Q!!S-D\S]``````]T-V66Q!!Q!!Q!!Q!![KK```"
		  db "``````ZG:Q!!Q!!Q!!Q!!Q!!Q!!Q!!T)SZG:Q!!Q!!YC)```"
		  db "`````````\O\V66Q!!Q!!Q!!U2%ZG:``````U.$Q!!U2%```"
	  

	  
;varr: dw 0x0000
	
;		_a_: dw 0x0008
;		_b_: dd irq_std
;		_c_: dw 0x8e00

	
;%define v_GDT_Limite 0x0020
;%define v_GDT_Base   0x0000000C
;%define v_IDT_Limite 0x0800
;%define v_IDT_Base   0x0000002C


;DTs_DEBUT :
;	GDTR_DEBUT :
;		gtdr_limite : dw 0x0020
;		gdtr_base   : dd 0x00000006
;	GDTR_FIN   :
;	GDT_DEBUT  :
;		gdttest    : db 0xA0, 0x00, 0x00, 0x80, 0x0b, 0x93, 0x40, 0x00
;		GDT_CS : db 0xFF, 0xFF, 0x00, 0x00, 0x00, 0x9B, 0x0D, 0x00
;		GDT_DS : db 0xFF, 0xFF, 0x00, 0x00, 0x00, 0x93, 0x0D, 0x00
;		GDT_SS : db 0x00, 0x00, 0x00, 0x00, 0x00, 0x97, 0x0D, 0x00
;	GDT_FIN    :
;
;	IDTR_DEBUT :
;		idtr_limite : dw 8
;		idtr_base   : dd IDT_DEBUT
;	IDTR_FIN   :
;	IDT_DEBUT  :
;		irq_01 : dw 0x0000, 0x0008, 0x008E, 0x0000
;		irq_02 : dw 0x0000, 0x0008, 0x008E, 0x0000
;		irq_03 : dw 0x0000, 0x0008, 0x008E, 0x0000
;		irq_04 : dw 0x0000, 0x0008, 0x008E, 0x0000
;		irq_05 : dw 0x0000, 0x0008, 0x008E, 0x0000
;		irq_06 : dw 0x0000, 0x0008, 0x008E, 0x0000
;		irq_07 : dw 0x0000, 0x0008, 0x008E, 0x0000
;		irq_08 : dw 0x0000, 0x0008, 0x008E, 0x0000
;	IDT_FIN    :
;	
;DTs_FIN :


;test : db 0xFFFFFFFFFFFFFFFF

;%define uint8  byte
;%define uint16 word
;%define uint32 dword

;%macro char 2

;struc rect
	
;	.x: resb 1
;	.y: resb 1
;	.largeur: resb 1
;	.hauteur: resb 1
;	.color: resb 1
	
;endstruc


;	%define %1(value) mov byte [ %1 ], value

;	%1 : db %2

;%endmacro

;%macro rect8 1

;	%define %1.setX( value )        mov uint8 [ %1.x ],        value
;	%define %1.setY( value )        mov uint8 [ %1.y ],        value
;	%define %1.setLargeur( value )  mov uint8 [ %1.largeur ],  value
;	%define %1.setHauteur( value )  mov uint8 [ %1.hauteur ],  value
;	%define %1.setAttribut( value ) mov uint8 [ %1.attribut ], value

;	%define %1.x( reg8 )        mov reg8, [ %1.x ]
;	%define %1.y( reg8 )        mov reg8, [ %1.y ]
;	%define %1.largeur( reg8 )  mov reg8, [ %1.largeur ]
;	%define %1.hauteur( reg8 )  mov reg8, [ %1.hauteur ]
;	%define %1.attribut( reg8 ) mov reg8, [ %1.attribut ]

;	%define %1.afficher          rect8.afficher %1

;	%1 :
;		%1.x        : db 0x00
;		%1.y        : db 0x00
;		%1.largeur  : db 0x00
;		%1.hauteur  : db 0x00
;		%1.attribut : db 0x00
;
;%endmacro





;%macro rect8.afficher 1

;	mov EBX, %1
;	call _m_afficherRect8_
;
;%endmacro


%macro __pthread__ 1
	%1 :
	
	%1._pre_task_: dw 0x0000
	%1._unused_pre_task_: dw 0x0000

	%1._esp0_       : dd 0x00000000
	%1._ss0_        : dw 0x0000
	%1._unused_ss0_ : dw 0x0000

	%1._esp1_       : dd 0x00000000
	%1._ss1_        : dw 0x0000
	%1._unused_ss1_ : dw 0x0000

	%1._esp2_       : dd 0x00000000
	%1._ss2_        : dw 0x0000
	%1._unused_ss2_ : dw 0x0000

	%1._cr3_    : dd 0x00000000
	%1._eip_    : dd 0x00000000
	%1._eflags_ : dd 0x00000000

	%1._eax_ : dd 0x00000000
	%1._ecx_ : dd 0x00000000
	%1._edx_ : dd 0x00000000
	%1._ebx_ : dd 0x00000000
	 
	%1._esp_ : dd 0x00000000
	%1._ebp_ : dd 0x00000000
	%1._esi_ : dd 0x00000000
	%1._edi_ : dd 0x00000000

	%1._es_        : dw 0x0000
	%1._unused_es_ : dw 0x0000

	%1._cs_        : dw 0x0000
	%1._unused_cs_ : dw 0x0000

	%1._ss_        : dw 0x0000
	%1._unused_ss_ : dw 0x0000

	%1._ds_        : dw 0x0000
	%1._unused_ds_ : dw 0x0000

	%1._fs_        : dw 0x0000
	%1._unused_fs_ : dw 0x0000

	%1._gs_        : dw 0x0000
	%1._unused_gs_ : dw 0x0000

	%1._ldt_ss_        : dw 0x0000
	%1._unused_ldt_ss_ : dw 0x0000

	%1._unused_io_mba_ : dw 0x0000
	%1._io_mba_        : dw 0x0000
	
	%1.end :
%endmacro

%macro __thread__ 1
	%1:
	%1._pre_ss_: resb 2
	%1._pre_esp_: resb 4
	%1._pre_cs_: resb 2
	%1._pre_eip_: resb 4
%endmacro

memcpy:
	add ecx, edx
	
	.cpy:
		mov al, [edx]
		mov [ebx], al
	
	.check:
		cmp edx, ecx
		inc edx
		inc ebx
		jb .cpy
	
	.end:
		ret


__pthread__ maineth

texte : db " Syst�me SEP version 1.0", CR, NUL
GDT_Chargee   : db ' - Global Descriptor Table fut charg�e.', CR, NUL
IDT_Chargee   : db ' - Interrupt Descriptor Table fut charg�e.', CR, NUL
PIC_Congigure : db ' - Programmable interrupt controller fut configur�.', CR, NUL
CPU_ID : resb 4
CPU_VENDOR_NAME : resb 13

;__tr__: dd 0x50, 0x00
;__tss__ : resb 100

taskTable:
;	task1
;	task2
;	task3

;__pthread__ maineth

; modeMask
%define modeMask 11000000b
	%define normal 00b
	%define stoped 01b
	
%define pl 00110000b
	%define kernel 00b
	%define system 01b
	%define root   10b
	%define user   11b

curTask: dw 0x0000
numTask: dw 0x0000

add.task:
	
	pushad
	push edx
	xor eax, eax
	xor ebx, ebx
	xor ecx, ecx

	mov eax, edx
	mov word ax, [numTask]
	
	mov ecx, 0x00000004
	mul ecx
	
	pop edx
	 
	mov [0x00020000 + eax], dword edx
	inc word [numTask]
	
	mov eax, [0x00020000]	
	
	popad
	
ret


%macro pthread 1
	%1:
		.eip : dd 0x00000000

		.ss  : dw 0x0000
		.esp : dd 0x00000000
	
		.eflags : dd 0x00000000
	
		.cs  : dw 0x0000
		;.eip : dd 0x00000000
		
		.ds : dw 0x0000
		.es : dw 0x0000
		.fs : dw 0x0000
		.gs : dw 0x0000
		
		.eax : dd 0x00000000
		.ebx : dd 0x00000000
		.ecx : dd 0x00000000
		.edx : dd 0x00000000
	.end:
%endmacro

pthread task1
pthread task2
pthread task3



;lecteurDisquette   : db "Lecteur de disquette ", NUL
;lecteurDisquette.1 : db "1 : ", NUL
;lecteurDisquette.2 : db "2 : ", NUL
;
;empty : db "vide", NUL
;full  : db "plein", NUL

;iniIDT :
;	mov [ EBX ], AL
;	inc EBX
;	mov [ EBX ], AH
;	inc EBX
;	shr EAX, byte 0x16
;
;	mov [ EBX ], CX
;	add EBX, byte 0x02
;
;	mov byte [ EBX ], 0x00
;	inc EBX
;	mov byte [ EBX ], 0x8E
;	inc EBX
;
;	mov [ EBX ], AL
;	inc EBX
;	mov [ EBX ], AH
;	inc EBX

;	ret

;val : db 0x75

;_oui : db "vrais", 0x00
;_non : db "faux", 0x00

;	_int.mov_ :
;		pop dword EAX
;		pop dword EBX
;		
;		mov dword [ EBX ], dword EAX
;		
;		ret

;iniidt:
;cli
;mov [ ebx ], ax
;and eax, 0xFFFF0000
;shr eax, 16
;mov [ ebx + 6 ], ax
;ret


main :



;_m_add_gdt_desc_ 0xffffffff, 0xffff, 0xdf93, 0x100000
;_m_add_gdt_desc_ 0xffffffff, 0xffff, 0xdf97

;	AfficherCaractere([testrerer])
;	AfficherCaractere([testrerer+1])
;	AfficherCaractere([testrerer+2])
;	AfficherCaractere([testrerer+3])
;	AfficherCaractere([testrerer+4])
;	AfficherCaractere([testrerer+5])
;	AfficherCaractere([testrerer+6])
;	AfficherCaractere([testrerer+7])


;	mov ax, 0x10        ; segment de donne
 ;   mov ds, ax
  ;  mov fs, ax
   ; mov gs, ax
;    mov es, ax
 ;   mov ss, ax


;	EtablirAttribut( 0x87 )

	xor eax, eax
	xor ebx, ebx
	xor ecx, ecx
	xor edx, edx
	
	mov ah, 0x71
	mov EBX, 0x00
	mov ECX, 0x7D0
	mov DL,  0x00
	
	call terminal.viderEcran

;	mov ebx, 0x730
;	call terminal.viderEcran

;	RemplirEcran ( 0x00, 0x00, 0x7D0, 0x87 )
;	RemplirEcran ( 0x00, 0x730, 0x7D0, 0x87 )

;	EtablirAttribut( 0x78 )
	mov ah, 0x17
	mov EBX, 0xA0
	mov ECX, 0x730

	call terminal.viderEcran

	mov [0xb809e], byte ':'
	mov [0xb809f], byte 0xf1

;	xor EBX, ebx
;	xor ecx, ecx
;
;	mov ah, 0x9F
;	mov EBX, 0x0dc4
;	mov ECX, 0x4c
;	
;	call terminal.viderEcran


	

;	RemplirEcran ( 0x00, 0xA0, 0x730, 0x78 )


;	mov AL, 0x00
;	mov EBX, 0xA0
;	mov ECX, 0x730
;	mov DL, 0x00
;	EtablirAttribut( 0x82 )
	mov [ terminal.vCurseur ], dword 0x000B8000

	gdt.init
	
	gdt.add.desc ( desc.cs, 1, 0x00000000, 0xFFFFF, 0xd9b )
	gdt.add.desc ( desc.ds, 2, 0x00000000, 0xFFFFF, 0xD93 )
	gdt.add.desc ( desc.ss, 3, 0x00000000, 0x00000, 0xD97 )
	;gdt.add.desc ( gdt,     4, 0x00000000, 0x00800, 0x493 )
	;gdt.add.desc ( idt,     5, 0x00000800, 0x00800, 0x493 )
	;gdt.add.desc ( video,   6, 0x000B8000, 0x00fa0, 0x493 )
	gdt.add.desc ( ucode,   4, 0x00100000, 0xfffff, 0xdff )
	gdt.add.desc ( udata,   5, 0x00100000, 0xfffff, 0xdf3 )
	gdt.add.desc ( ustack,  6, 0x00100100, 0x00000, 0xdf7 )
	gdt.add.desc ( tss,     7, maineth,    0x68,    0xe9 )


	lgdt [ __GDTR_LIMITE__ ]
	
	mov ax, 0x10
	mov ds, ax
	mov es, ax
	mov fs, ax
	mov gs, ax

;	jmp 0x8:gdt.init.fin
	
;	gdt.init.fin:
	
	mov ax, 0x18
	mov ss, ax
	mov esp, 0x0002fff1
	
;	cmp [ __idtr_limit__ ], 0x0107
	
	
;	mov eax, 0xffffffff
;	mov bx,  0xFFFF
;	mov cx,  0xDF9B
;	call gdt.add_desc
;	
;	mov eax, 0xffffffff
;	mov bx,  0xFFFF
;	mov cx,  0xDF93
;	call gdt.add_desc
;	
;	mov eax, 0xffffffff
;	mov bx,  0xFFFF
;	mov cx,  0xDF97
;	call gdt.add_desc

;	mov ax, 0x0008
;	mov cs, ax
;    mov ds, ax
;    mov fs, ax
;    mov gs, ax
;    mov es, ax
;    mov ss, ax




;	mov ax, video
;	mov gs, ax
;	push edi
;	mov edi, 0x0000009e
;	xor eax, eax
;	mov [gs:edi], byte 'a'
;	add edi, 2
;	mov [gs:edi], byte 'l'
;	mov ax, [0x00000800]
	
;	cmp ax, 0x00
;	je egal
;	AfficherCaractere(al)
;	egal:
;	AfficherTexte(texte)
	
;	mov 
;		call __idt.init__
	xor edx, edx
	iniidt:
		push word  0x0008
		push dword irq.std
		push word  0x8e00
		mov edx, 0x00000800
		iniidt.call:
			call __idt.add.desc__
			
			cmp edx, 0x1000
			jb iniidt.call

		push word  0x0008
		push dword irq.00
		push word  0x8e00
		mov edx, 0x00000900
		call __idt.add.desc__
	
		push word  0x0008
		push dword irq.01
		push word  0x8e00
		call __idt.add.desc__

		push word  0x0008
		push dword irq.gp
		push word  0x8e00
		mov edx, 0x00000868
		call __idt.add.desc__
		
		push word  0x0008
		push dword irq.dbg
		push word  0x8e00
		mov edx, 0x00000808
		call __idt.add.desc__

		push word  0x0008
		push dword irq.pf
		push word  0x8e00
		mov edx, 0x00000830
		call __idt.add.desc__

		push word  0x0008
		push dword irq.nw
		push word  0x8e00
		mov edx, 0x00000828
		call __idt.add.desc__

		push word  0x0008
		push dword isr.afficherTexte
		push word  0x8f00
		mov edx, 0x00000950
		call __idt.add.desc__

;		push word  0x0008
;		push dword isr.afficherTexte
;		push word  0xef00
;		mov edx, 0x00000958
;		call __idt.add.desc__
;42




;		pop cx
;		pop ebx
;		mov ebx, irq_01
;		push ebx
;		push cx
;		call __idt.add.desc__
;		mov dl, [0x00000ff8]
;		mov [0xb8001], dl
;		mov dl, [0x00000ff9]
;		mov [0xb8003], dl
;		mov dl, [0x00000ffa]
;		mov [0xb8005], dl
;		mov dl, [0x00000ffb]
;		mov [0xb8007], dl
;		mov dl, [0x00000ffc]
;		mov [0xb8009], dl
;		mov dl, [0x00000ffd]
;		mov [0xb800b], dl
;		mov dl, [0x00000ffe]
;		mov [0xb800d], dl
;		mov dl, [0x00000fff]
;		mov [0xb800f], dl


;		mov dl, [0x2fff0]
;		mov [0xb8001], dl
;		mov dl, [0x2ffef]
;		mov [0xb8003], dl
;		mov dl, [0x2ffee]
;		mov [0xb8005], dl
;		mov dl, [0x2ffed]
;		mov [0xb8007], dl
;		mov dl, [0x2ffec]
;		mov [0xb8009], dl
;		mov dl, [0x2ffeb]
;		mov [0xb800b], dl
;		mov dl, [0x2ffea]
;		mov [0xb800d], dl
;		mov dl, [0x2ffdf]
;		mov [0xb800f], dl


;		mov eax, irq_std
;		mov [0xb8013], al
;		mov [0xb8015], ah
;
;		shr eax, 16
;		mov [0xb8017], al
;		mov [0xb8019], ah
;
;
;		AfficherCaractere ( 'A' )
;		AfficherCaractere ( 'A' )
;		AfficherCaractere ( 'A' )
;		AfficherCaractere ( 'A' )
;		AfficherCaractere ( 'A' )
;		AfficherCaractere ( 'A' )
;		AfficherCaractere ( 'A' )
;		AfficherCaractere ( 'A' )
;		AfficherCaractere ( 'A' )
;		AfficherCaractere ( 'A' )
;		AfficherCaractere ( 'A' )
;		AfficherCaractere ( 'A' )
;		AfficherCaractere ( 'A' )
;		AfficherCaractere ( 'A' )
;		AfficherCaractere ( 'A' )
;		AfficherCaractere ( 'A' )
		

;push word 0x2400
;pop ax

;AfficherCaractere (ah)
;AfficherCaractere (0x24)

_pic_config_ :
		ICW :
		    ICW_1 :
		        mov AL, 0x11
		        out 0x20, AL
		        out 0xA0, AL
		    jmp ICW_2

		    ICW_2 :
		        mov AL, 0x20
		        out 0x21, AL
		        mov AL, 0x70
		        out 0xA1, AL
		    jmp ICW_3

		    ICW_3 :
		        mov AL, 0x04
		        out 0x21, AL
		        mov AL, 0x02
		        out 0xA1, AL
		    jmp ICW_4

		    ICW_4 :
		        mov AL, 0x01
		        out 0x21, AL
		        out 0xA1, AL
		    jmp ICW_FIN

		ICW_FIN :

		OCW :
		    OCW_1 :
		        mov AL, 0x00
		        out 0x21, AL
				mov al, 0x00
		        out 0xA1, AL
		    jmp OCW_FIN

		OCW_FIN :

PIT:
	mov dx, 1193180/100
	
	mov	al, 00110110b
	out	0x43, al

	mov	ax, dx
	out	0x40, al	;LSB
	xchg ah, al
	out	0x40, al	;MSB

		lidt [ __idt.init__ ]
		
		AfficherTexte (texte)

		sti
		
;		mov al, 11000010b
;		out 0x92, al
		
		AfficherTexte (GDT_Chargee)
		AfficherTexte (IDT_Chargee)
		AfficherTexte (PIC_Congigure)
		
		call __Processor_Check__
		
		AfficherCaractere('E')
		
		
		sti
		
		
;		xor eax, eax
;		cpuid
;		mov [ CPU_ID ],              eax
;		mov [ CPU_VENDOR_NAME ],     ebx
;		mov [ CPU_VENDOR_NAME + 4 ], edx
;		mov [ CPU_VENDOR_NAME + 8 ], ecx
;		
		mov eax, 0x00000001
		cpuid
		
		; VBE
		mov dx, 0x1ce
		mov ax, 4
		out dx, ax
		
		mov dx, 0x1cf
		mov ax, 0x00
		out dx, ax
		
		mov dx, 0x1ce
		mov ax, 1
		out dx, ax
		
		mov dx, 0x1cf
		mov ax, 32
		out dx, ax
		
		mov dx, 0x1ce
		mov ax, 2
		out dx, ax
		
		mov dx, 0x1cf
		mov ax, 32
		out dx, ax

		mov dx, 0x1ce
		mov ax, 3
		out dx, ax
		
		mov dx, 0x1cf
		mov ax, 0x18
		out dx, ax
		
		mov dx, 0x1ce
		mov ax, 4
		out dx, ax
		
		mov dx, 0x1cf
		mov ax, 0x81
		out dx, ax
		
		xor eax, eax
		xor ebx, ebx
		xor ecx, ecx
		xor edx, edx
		mov edx, 0xa0000
		mov ecx, picture
		
		mov eax, 0x00

		img:
			xor eax, eax
			mov eax, [ecx]

			xchg al, ah
			
			mov [edx], eax
			
			add ebx, 3
			add edx, 3
			add ecx, 3

			cmp ebx, 3072
			jbe img
			
			jmp nextstep

			mov eax, [ecx]
			mov [edx], eax
			add ebx, 4
			add edx, 4
			add ecx, 4
			mov al, [ecx]
			mov [edx], al
			add ebx, 1
			add edx, 1
			add ecx, 1
			
			mov edx, 0xa0060
			
			mov al, [ecx]
			mov [edx], al
			add ebx, 4
			add edx, 4
			add ecx, 4
			mov eax, [ecx]
			mov [edx], eax
			add ebx, 4
			add edx, 4
			add ecx, 4
			mov al, [ecx]
			mov [edx], al
			add ebx, 1
			add edx, 1
			add ecx, 1
			
			mov edx, 0xa0090

			mov eax, [ecx]
			mov [edx], eax
			add ebx, 4
			add edx, 4
			add ecx, 4
			mov eax, [ecx]
			mov [edx], eax
			add ebx, 4
			add edx, 4
			add ecx, 4
			mov al, [ecx]
			mov [edx], al
			add ebx, 1
			add edx, 1
			add ecx, 1
			
			mov edx, 0xa00c0

			mov eax, [ecx]
			mov [edx], eax
			add ebx, 4
			add edx, 4
			add ecx, 4
			mov eax, [ecx]
			mov [edx], eax
			add ebx, 4
			add edx, 4
			add ecx, 4
			mov al, [ecx]
			mov [edx], al
			add ebx, 1
			add edx, 1
			add ecx, 1
			
			mov edx, 0xa00f0

			mov eax, [ecx]
			mov [edx], eax
			add ebx, 4
			add edx, 4
			add ecx, 4
			mov eax, [ecx]
			mov [edx], eax
			add ebx, 4
			add edx, 4
			add ecx, 4
			mov al, [ecx]
			mov [edx], al
			add ebx, 1
			add edx, 1
			add ecx, 1
			
			mov edx, 0xa0120

			mov eax, [ecx]
			mov [edx], eax
			add ebx, 4
			add edx, 4
			add ecx, 4
			mov eax, [ecx]
			mov [edx], eax
			add ebx, 4
			add edx, 4
			add ecx, 4
			mov al, [ecx]
			mov [edx], al
			add ebx, 1
			add edx, 1
			add ecx, 1
			
			mov edx, 0xa0150

			mov eax, [ecx]
			mov [edx], eax
			add ebx, 4
			add edx, 4
			add ecx, 4
			mov eax, [ecx]
			mov [edx], eax
			add ebx, 4
			add edx, 4
			add ecx, 4
			mov al, [ecx]
			mov [edx], al
			add ebx, 1
			add edx, 1
			add ecx, 1
			

;			cmp ebx, 196608
;			jbe img
		
		; Lire sur un disque dur
		
			; Init
			
				; DMA
				
			;	out 0x0a, 0x06
			;	out 0xd8, 0xff
			;	out 0x04,
			jmp nextstep
			
			__read_stats__ :
				xor eax, eax
				mov dx, 0x1f7
				in al, dx
				
				and al, 0x08
				je __read_stats__
				
				AfficherTexte(texte)

				ret
		
		%macro callf 1
		
			push dword %%curPos
		
		;	%rep (%0 - 1)
		;		%rotate -1
		;		push %1
		;	%endrep
			
			jmp %1
			
			%%curPos:
			
		%endmacro

;		%macro return 1
;		
;			pop eax
;		
;			push %1
;			
;			jmp eax
;			
;		%endmacro
		
		%macro return 0
		
			pop eax
			
			jmp eax
			
		%endmacro
				
		ctnu:

			mov dx, 0x1f1
			mov al, 0x00
			out dx, al

			mov dx, 0x1f2
			mov al, 0x01
			out dx, al
		
			mov dx, 0x1f3
			mov al, 0x00
			out dx, al
		
			mov dx, 0x1f4
			mov al, 0x00
			out dx, al
		
			mov dx, 0x1f5
			mov al, 0x00
			out dx, al
		
			mov dx, 0x1f6
			mov al, 0xe0
			out dx, al
		
			
			mov dx, 0x1f7
			mov al, 0x30
			out dx, al
			
			AfficherTexte(texte)
			
			call __read_stats__

			AfficherTexte(texte)

			xor ecx, ecx
			xor edx, edx
			xor eax, eax
			mov dx, 0x1f0
			__read_data__ :
				out dx, ax
				
				inc ax
				add ecx, 2
				
				cmp ecx, 512
				jbe __read_data__
			
			
			jmp nextstep

			ret
			
			
			[section .bss]
			
			Part1: resb 16
			
			[section .text]
			
		nextstep:
			
;		mov eax, edx
;		and edx, 0x00000001
		;je near beforewait

;		push edx
;		push ecx
;		push ebx
;		;mov eax, edx
;		mov ch, 0x2
;		call hex
		
;		mov edx, eax
;		and edx, 0x0000002
;		;je near beforewait
;		
;		
;		pop eax
;		;mov eax, ebx
		mov ch, 0x2
		call hex
		jmp beforewait
;
;		pop eax
;		;mov eax, ebx
;		mov ch, 0x2
;		call hex
;
;		pop eax
;		;mov eax, ebx
;		mov ch, 0x2
;		call hex
;		
;		xor eax, eax
;		xor ebx, ebx
;		xor ecx, ecx
;		xor edx, edx
		
;		mov dx, 0x03c4
;		mov al, 0x7
;		out dx, al
;
;		mov dx, 0x03c5
;		mov al, 0x05
		;out dx, al
		
		AfficherCaractere(CR)
		
		jmp rdsstats
		
		conter : db 0x00


		rdsstats:

		mov dx, 0x03d4
		mov al, [conter]	
		out dx, al

		mov dx, 0x03d5
		in al, dx

		mov ch, 0x00
		call hex
		AfficherCaractere(' ')
		
		xor eax, eax
		
		inc byte [conter]
	
		cmp [conter], byte 0x27
		jb rdsstats

		AfficherCaractere(CR)
		AfficherCaractere(CR)
		
		mov [conter], byte 0x00
		
		jmp rdsstatssr
		
		;conter : db 0x00


		rdsstatssr:

		mov dx, 0x03c4
		mov al, [conter]	
		out dx, al

		mov dx, 0x03c5
		in al, dx

		mov ch, 0x00
		call hex
		AfficherCaractere(' ')
		
		xor eax, eax
		
		inc byte [conter]
	
		cmp [conter], byte 0x6
		jb rdsstatssr

		AfficherCaractere(CR)
	
			
;		mov dx, 0x03c4
;		mov al, 0x01
;		out dx, al
;
;		mov dx, 0x03c5
;		mov al, 0x39
;		out dx, al		
;		
;		mov dx, 0x03c2
;		mov al, 0xef
;		out dx, al
;			
;		mov dx, 0x03ce
;		mov al, 0x06
;		out dx, al
;
;		mov dx, 0x03cf
;		mov al, 0x0d
;		out dx, al		
;
;		mov dx, 0x03ce
;		mov al, 0x05
;		out dx, al
;
;		mov dx, 0x03cf
;		mov al, 0x53
;		out dx, al		
;		
;		
;		mov dx, 0x03d4
;		mov al, 0x01
;		out dx, al
;
;		mov dx, 0x03d5
;		mov al, 0x7f
;		out dx, al
;
;		mov dx, 0x03d4
;		mov al, 0x02
;		out dx, al
;
;		mov dx, 0x03d5
;		mov al, 0x6f
;		out dx, al
;
;		mov dx, 0x03d4
;		mov al, 0x03
;		out dx, al
;
;		mov dx, 0x03d5
;		mov al, 0x09
;		out dx, al
;
;		mov dx, 0x03c4
;		mov al, 0x1
;		out dx, al

;		mov dx, 0x03c5
;		mov al, 0x05
;		out dx, al

		
		mov ch, 0x00
		call hex

		;mov dx, 0x03ce
		;mov al, 0x5
		;out dx, al

		;mov dx, 0x03cf
		;mov al, 0x70
		;out dx, al

		
;		mov eax, 7589030
;		mov ecx, 0x000B810A
;		mov ebx, 10
;		
;		decimal:
;		xor edx, edx
;		div ebx
;		add edx, 48
;		mov [ecx], dl
;		sub ecx, 2
;		cmp eax, 0x00
;		jne decimal
		
		
;	mov eax, 0xfc25d89a
;	mov ch, 0x02
	
;	call hex
	
xor eax, eax

mov edx, maine
mov ebx, 0x00100000
mov ecx, 100
call memcpy

mov edx, maine
mov ebx, 0x00100000
mov ecx, 100

call memcpy
mov edx, maine
mov ebx, 0x00100000
mov ecx, 100
call memcpy

;	mov ax, 0x28
;	mov ds, ax

	;mov ax, ds
	;or ax, 0x0003
	;mov ds, ax

	;mov ax, ss
	;or ax, 0x0003
	;mov ss, ax

;	mov ax, cs
;	or ax, 0x0003
;	mov cs, ax

	;mov byte [0xb8200], 'r'





;	mov [task1.ss],     word  0x0018
;	mov [task1.esp],    dword esp
;	mov [task1.eflags], dword eax
;	mov [task1.cs],     word  0x0008
;	mov [task1.eip],    dword 0x00100000
;
;	mov [task2.ss],     word  0x0018
;	mov [task2.esp],    dword esp
;	mov [task2.eflags], dword eax
;	mov [task2.cs],     word  0x0008
;	mov [task2.eip],    dword 0x00100000
;
;	mov [task3.ss],     word  0x0018
;	mov [task3.esp],    dword esp
;	mov [task3.eflags], dword eax
;	mov [task3.cs],     word  0x0008
;	mov [task3.eip],    dword 0x00100000
;
;	xor edx, edx
;
;	mov edx, task1
;	call add.task

;	mov edx, task2
;	call add.task
;
;	mov edx, task3
;	call add.task
;
















	jmp nxt
testcall:
pop dword eax
trrttrrt:
push dword eax

mov ch, 0x02
callhex:
xor edx, edx
xor ebx, ebx
call hex
testcall.3:
pop dword ebx
pop dword eax
push dword eax
push dword ebx
testcall.2:
mov ch, 0x02
call hex
testcall.1:
;ret

	nxt:

pushfd
pop dword eax
mov ch, 0x2
call hex



AfficherTexte(texte)

;mov ax, 0x8
;mov cs, ax

;AfficherCaractere('0')

;	push ss
;	push esp
;	pushfd
;	push cs
;	push beforewait


;jmp 0x38:0x00000000
;AfficherCaractere('R')
;call testcall

;jmp beforewait

createCallGate:
cli
	;AfficherCaractere('t')

	pushfd
	pop dword eax
	
	or eax, 0x00000200
	push dword eax
	;popfd

	mov [maineth._esp0_],		  dword esp
	mov [maineth._ss0_],		  word  ss
	mov [maineth._eip_],		  dword 0x00000000
	mov [maineth._eflags_],		  dword 0x00000200
	mov [maineth._unused_io_mba_], word  0x0001	
	mov [maineth._cs_],			  word  0x0023
	mov [maineth._ss_],			  word  0x33
	mov [maineth._esp_],		  dword esp
	mov [maineth._ds_],			  word  0x28
;	AfficherCaractere('5')

	;AfficherCaractere('t')
	;AfficherCaractere('5')
;	xor eax, eax
	mov ax, 0x38
;	ltrinst:
;	ltr ax
	AfficherCaractere('5')
	;jmp beforewait

;push eax
;popfd

;push word 0x33
;push dword 0x00020000

;push dword 0x00000200
;pop dword eax
;push dword eax

;push word 0x0023
;push dword 0x00000000
	
;	AfficherCaractere('5')

;mov ax, 0x2b
;mov ds, ax

;mov [0x00000050], word 0x56ac

;mov ax, 0x10
;mov ds, ax

;mov ax, [0x00100050]
;mov [0xb8050], ax

;mov [0xb8052], word 0x56ac

;sti

;iretd

















;mov ax, 0x30
;mov ss, ax
;mov ebx, esp
;mov esp, 0x00000000

;push word 0x8976

;mov esp, ebx
;mov ax, 0x18
;mov ss, ax

;AfficherCaractere('�')

;sti

;iretd




;AfficherCaractere('R')

;mov ax,  [maineth._ss_]
;mov edi, [maineth._esp_]
;	AfficherCaractere('5')
;mov ss,  ax
;mov esp, edi
;push 0x36
;push 0x00
;pushf
;push 0x31
;push 0x00
;AfficherCaractere('R')

;jmp 0x1c:0x00000000

;AfficherCaractere('R')

;mov word [threadtest._pre_ss_],  0x18
;mov dword [threadtest._pre_esp_], esp
;mov word [threadtest._pre_cs_],  0x08
;mov dword [threadtest._pre_eip_],  beforewait
;AfficherCaractere('R')
;AfficherCaractere('J')

;mov ax, 0x2b
;mov ds, ax
;mov es, ax
;mov ax, 0x23
;mov fs, ax
;mov gs, ax
;mov fs, ax
;mov gs, ax
;AfficherCaractere(' ')

;mov ss, ax
;mov esp, edi

;		mov [0xb8000], dword 0x00ff00
;		mov [0xb8004], dword 0xff00ff
;		mov [0xb8008], dword 0x00ffff


;iret

;sti

;jmp dword 0x23:0x00000000

;jmp 0xb:0x00030000
nextiret:
;AfficherCaractere('R')
;mov edx, 0x100000
;mov ebx, 0xb8050
;mov ecx, 50

;call memcpy




;		mov al, 0x06
;		out 0x0a, al
;
;		mov al, 0xff
;		out 0xd8, al
;
;		mov al, 0x80
;		out 0x04, al
;
;		mov al, 0xb8
;		out 0x04, al
;
;		mov al, 0xff
;		out 0xd8, al
;
;		mov al, 0xa0
;		out 0x05, al
;
;		mov al, 0x0f
;		out 0x05, al
;
;		mov al, 0x00
;		out 0x80, al
;
;		mov al, 0x02
;		out 0x0a, al
;
;
;		mov al, 0x06
;		out 0x0a, al
;
;		mov al, 0x56
;		out 0x0b, al
;
;		mov al, 0x02
;		out 0x0a, al


;		int 3







	;pop edi
	
;	uint8_t var8
;	uint16_t var16
;	uint24_t var24
;	utexte_t txt, { 'Chaine de caract�re de test!', CR }
;    variable.set( byte 0xFF )
;    var.set( 0xAF )
;	mov CL, 2
;	__mul__ __8_b__, var.ptr, CL
	
	
;	var8.set ( 200 )
;	var8.sub ( 100 )
;	var8.get ( AH )
	
;	mov AL, 0xff
;	mov ECX, [ _int_v_variable8_ ]
;
;	cmp ECX, 8500
;	jz __oui
;
;	AfficherTexte ( _non )
;	jmp _endd
;
;	__oui :
;		AfficherTexte ( _oui )
;
;	_endd :
;
;	AfficherCaractere ( CR )
;	AfficherTexte ( txt.ptr )

;	call terminal.viderEcran


;	xor EAX, EAX
;	xor EBX, EBX
;	mov BX, 0x0002
;
;	mov AX, 0x00A0
;	sub AX, 0x002E
;
;	div BX
;	xor EBX, EBX
;	mov BX, AX
;
;	add EBX, 0x000B80A0
;
;	mov [ terminal.vCurseur ], EBX

;	xor EAX, EAX
;	xor EBX, EBX
;	xor ECX, ECX
;	xor EDX, EDX


;	mov AL, 0x10
;	out 0x70, AL
;
;	xor AX, AX
;
;	in AL, 0x71
;
;	uint8_t dev.floppy_1
;	mov [ dev.floppy_1_ref ], AL
;	uint8_t dev.floppy_2, 0x00
;
;	mov AH, [ dev.floppy_1_ref ]
;
;	cmp AH, 0x00
;	je nofloppy
;
;	floppy :
;		AfficherTexte ( lecteurDisquette )
;		AfficherTexte ( lecteurDisquette.1 )
;		AfficherTexte ( full )
;	jmp floppyend
;
;	nofloppy :
;		AfficherTexte ( lecteurDisquette )
;		AfficherTexte ( lecteurDisquette.1 )
;		AfficherTexte ( empty )
;
;	floppyend :
;
;
;
;AfficherCaractere ( CR )


;	tst :
;
;	mov BX, 0x0000
;	mov AX, 0011000101110001b
;
;	bt AX, BX
;
;	jc v1
;	jmp v0
;
;
;	v0 :
;		AfficherCaractere ( '0' )
;	jmp cnt
;	v1 :
;		AfficherCaractere ( '1' )
;
;	cnt :
;		cmp BX, 0x08
;		je tend
;		inc BX
;	jmp tst
;
;	tend :

;	%define TERMINAL_INC_INIT
;	%ifndef TERMINAL_INC_INIT
;	%define TERMINAL_INC_INIT
;
;		xor EAX, EAX
;		xor EBX, EBX
;		xor EDX, EDX
;
;		mov DX, 0x03D4 ; Affecte la valeur 0x03D4 au registre DX.
;		mov AL, 0x0E   ; Affecte la valeur 0x0E au registre AL.
;		out DX, AL     ; Envoie de AL au port de sortie DX.
;
;		mov DX, 0x03D5 ; Affecte la valeur 0x03D5 au registre DX.
;		in  AL, DX     ; Entre la valeur du port DX dans AL.
;		mov BH, AL     ; Affecte le registre AL au registre BH.
;
;		mov DX, 0x03D4 ; Affecte la valeur 0x03D4 au registre DX.
;		mov AL, 0x0F   ; Affecte la valeur 0x0F au registre AL.
;		out DX, AL     ; Envoie de AL au port de sortie DX.
;
;		mov DX, 0x03D5 ; Affecte la valeur 0x03D5 au registre DX.
;		in  AL, DX     ; Entre la valeur du port DX dans AL.
;		mov BL, AL     ; Affecte le registre AL au registre BL.
;
;		mov EAX, EBX
;		mov EBX, 0x00000002
;		mul EBX
;		add EAX, 0x000B8000
;
;		mov [ terminal.vCurseur ], EAX
;
;	%endif ; TERMINAL_INC_INIT





;	AfficherTexte ( texte )
;	AfficherTexte ( texte )
;
;
;
;	iniDTs :
;		_ini_reg_ :
;			xor EAX, EAX
;			xor EBX, EBX
;			xor ECX, ECX
;
;		_copie_gdtr_ :
;			mov word [ EBX ], word v_GDT_Limite
;			add EBX, byte 0x02
;			mov dword [ EBX ], dword v_GDT_Base
;			add EBX, byte 0x04
;
;		_copie_idtr_ :
;			mov word  [ EBX ], v_IDT_Limite
;			add EBX, byte 0x02
;			mov dword [ EBX ],   v_IDT_Base
;			add EBX, byte 0x04
;
;		_copie_gdt_ :
;			mov AL, byte [ GDT_DEBUT + ECX ]
;			mov [ EBX ], AL
;
;			cmp CX, v_GDT_Limite
;
;			inc EBX
;			inc ECX
;
;			jb _copie_gdt_
;
;		_copie_idt_ :
;
;			xor EAX, EAX
;
;			_copie_idt_code_ :

;				_m_copie_idt_ IRQ_0
;
;				cmp AL, 0xFF
;
;				inc AL
;
;				jb _copie_idt_code_
;
;
;		_charge_gdt_ :
;			lgdt [ 0xffffffff ]
;
;			AfficherTexte ( GDT_Chargee )

;		_charge_idt_ :
;			lidt [ 0x00000006 ]
;
;			AfficherTexte ( IDT_Chargee )
;_pic_config_ :
;		ICW :
;		    ICW_1 :
;		        mov AL, 0x11
;		        out 0x20, AL
;		        out 0xA0, AL
;		    jmp ICW_2
;
;		    ICW_2 :
;		        mov AL, 0x20
;		        out 0x21, AL
;		        mov AL, 0x70
;		        out 0xA1, AL
;		    jmp ICW_3
;
;		    ICW_3 :
;		        mov AL, 0x04
;		        out 0x21, AL
;		        mov AL, 0x02
;		        out 0xA1, AL
;		    jmp ICW_4
;
;		    ICW_4 :
;		        mov AL, 0x01
;		        out 0x21, AL
;		        out 0xA1, AL
;		    jmp ICW_FIN
;
;		ICW_FIN :
;
;		OCW :
;		    OCW_1 :
;		        mov AL, 0x00
;		        out 0x21, AL
;		        out 0xA1, AL
;		    jmp OCW_FIN
;
;		OCW_FIN :
;
;		sti

;	 EtablirAttribut 01000111b

;    mov dword [ terminal.vCurseur ], 0x000B8AA0


;	call Cirrus_Logic_Test_Debut

;jmp dword 0x0020:0x00000000

;te:
;	rect8 MENU
;
;
;	MENU.setX        ( uint8 5 )
;
;
;	mov [ MENU.y ], byte 0x01
;
;
;	MENU.setLargeur  ( uint8 10 )
;
;
;	MENU.setHauteur  ( uint8 5 )
;
;
;	MENU.setAttribut ( uint8 0xF8 )
;
;
;	y : db 0x01
;
;	MENU.afficher
beforewait:
xor eax, eax
sti
.hlt
;AfficherCaractere('H')
hlt
pop eax
inc eax
mov dword [0xa0000], eax
push eax
jmp .hlt

%define key.backspace 0x08
%define key.esc 0x1b
%define key.enter 0x0d
%define key.ctrl 0x00
%define key.lshift 0x0e
%define key.rshift 0x0f
%define key.alt 0x00
%define key.tab 0x09
%define key.capsLock 0x00
%define key.f1 0x00
%define key.f2 0x00
%define key.f3 0x00
%define key.f4 0x00
%define key.f5 0x00
%define key.f6 0x00
%define key.f7 0x00
%define key.f8 0x00
%define key.f9 0x00
%define key.f10 0x00
%define key.numLock 0x00
%define key.scrollLock 0x00
%define key.home 0x00
%define key.up 0x00
%define key.pageUp 0x00
%define key.grey_ow 0x00
;%define key.

charsmap: db 0x0, '1', '2', '3', '4', '5', '6', '7', '8', '9'
		  db '0', '-', '=', 0x0, 0x0, 'q', 'w', 'e', 'r', 't'
		  db 'y', 'u', 'i', 'o', 'p', '^', '�', CR,  0x0, 'a'
		  db 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', ';', '�'
		  db '�', '/', '�', 'z', 'x', 'c', 'v', 'b', 'n', 'm'
		  db ',', '.', '�', 0x0, '*', 0x0, ' ', 0x0, 0x0, 0x0
		  db 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0
	      db '7', '8', '9', '-', '4', '5', '6', '+', '1', '2'
		  db '3', '0', '.', 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0
		  db 0x0, 0x0, '7', '8', '9', '-', '4', '5', '6', '+'
		  db '1', '2', '3', '0', '.', 0x0, 0x0, 0x0, 0x0, 0x0
		  db 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0
		  db 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0

specsmap: db 0x0, '1', '2', '3', '4', '5', '6', '7', '8', '9'
		  db '0', '-', '=', 0x0, 0x0, 'q', 'w', 'e', 'r', 't'
		  db 'y', 'u', 'i', CR , 'p', '^', '�', CR , 0x0, 'a'
		  db 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', ';', '�'
		  db '�', 0x0, '�', 'z', 'x', 'c', 'v', 'b', 'n', 'm'
		  db ',', '.', '/', 0x0, '*', 0x0, ' ', 0x0, 0x0, 0x0
		  db 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0
	      db '7', '8', '9', '-', '4', '5', '6', '+', '1', '2'
		  db '3', '0', '.', 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0
		  db 0x0, 0x0, '7', '8', '9', '-', CR , '5', '6', '+'
		  db '1', '2', '3', '0', '.', 0x0, 0x0, 0x0, 0x0, 0x0
		  db 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0
		  db 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0


interupt:
	irq.std:
		AfficherCaractere('t')
	
		mov al, 0x20
		out 0x20, al
		
		iret

	irq.gp:
		xor eax, eax
		xor ebx, ebx

		pop dword eax
		pop word bx
		push word bx
		push dword eax
		
		xchg eax, ebx
		mov ch, 0x01
		call hex
		AfficherCaractere(':')
		
		pop dword eax
		pop word bx
		push word bx
		push dword eax
		
		mov ch, 0x02
		call hex
		AfficherCaractere(' ')
		
		AfficherCaractere('g')
	
		mov al, 0x20
		out 0x20, al
		
		iret

	irq.pf:
		xor eax, eax
		xor ebx, ebx

		pop dword eax
		pop word bx
		push word bx
		push dword eax
		
		xchg eax, ebx
		mov ch, 0x01
		call hex
		AfficherCaractere(':')
		
		pop dword eax
		pop word bx
		push word bx
		push dword eax
		
		mov ch, 0x02
		call hex
		AfficherCaractere(' ')
		
		AfficherCaractere('p')
	
		mov al, 0x20
		out 0x20, al
		
		iret

	irq.nw:
		xor eax, eax
		xor ebx, ebx

		pop dword eax
		pop word bx
		push word bx
		push dword eax
		
		xchg eax, ebx
		mov ch, 0x01
		call hex
		AfficherCaractere(':')
		
		pop dword eax
		pop word bx
		push word bx
		push dword eax
		
		mov ch, 0x02
		call hex
		AfficherCaractere(' ')
		
		AfficherCaractere('n')
	
		mov al, 0x20
		out 0x20, al
		
		iret

	irq.dbg:
	pushad
		xor eax, eax
		xor ebx, ebx

		pop dword eax
		pop word bx
		push word bx
		push dword eax
		
		xchg eax, ebx
		mov ch, 0x01
		call hex
		AfficherCaractere(':')
		
		pop dword eax
		pop word bx
		push word bx
		push dword eax
		
		mov ch, 0x02
		call hex
		AfficherCaractere(' ')
		
		AfficherCaractere('d')
		AfficherCaractere('b')
		AfficherCaractere('g')
	
		mov al, 0x20
		out 0x20, al
		popad
		iret
		
	stackSwitch:
		call eax
		
		iret
		
	;-----------------------------------------------------------------------------------------------	
		
	irq.00.count: db 0x00
	irq.00.sec: db 0x00
	irq.00.min: db 0x00
	irq.00:		
		
		;AfficherCaractere('C')
		
		.addCount:
			inc byte [irq.00.count]
			cmp byte [irq.00.count], 100
			jb .showTime

		.addSec:
			mov byte [irq.00.count], 0x00
			inc byte [irq.00.sec]
			cmp byte [irq.00.sec], 59

		.addMin:
			

		.addHrs:
			

		.showTime:
			mov ah,  [irq.00.sec]
			mov [0xb80a0], ah

		.end:
		
		;ordonanceur:
		;AfficherCaractere('C')
		;xor eax, eax
		;xor ebx, ebx
		;xor ecx, ecx
	
		;mov eax, task1
		;mov edx, task2
	
		;mov word eax, [curTask]
		;mov ecx, 0x00000004
		;mul ecx
		;add eax, 0x00020000
	
;	mov bx,  [eax]
;	mov es, bx
;	mov ecx, [eax + 2]
;	mov esi, ecx
	
	;	mov al, 0x20
	;	out 0x20, al

	;jmp ecx

	;add eax, 4
	
	;mov bx, [eax]
	;push bx
	;mov ebx, [eax + 2]
	;push ebx
	;mov ebx, [eax + 6]
	;push ebx
	;mov bx, [eax + 10]
	;push bx
	;mov ebx, [eax + 12]
	;push ebx
	
;	.read:
		;add eax, 0x00020000
		;mov ebx, [eax]
		;inc word [curTask]
;		cmp ebx, 0x00000000
;		ja .trfs
		;	AfficherCaractere('C')

;		mov word [curTask], 0x0001
;		mov eax, 0x00000001
;		jmp .read
		
;	.trfs:
		;AfficherCaractere('C')
	
		;mov eax, ebx
		;mov ch, 0x02
		;call hex
	
;		mov ax,  [ebx]
;		push word ax
;		mov eax, [ebx + 2]
;		push dword ax

;		mov eax, [ebx + 6]
;		push dword ax
		
;		mov ax,  [ebx + 10]
;		push word ax
;		mov eax, [ebx + 12]
;		push dword eax
		
;		mov ax, [ebx + 16]
;		mov ds, ax
;		mov ax, [ebx + 18]
;		mov es, ax
;		mov ax, [ebx + 20]
;		mov fs, ax
;		mov ax, [ebx + 22]
;		mov gs, ax

;		mov eax, [ebx + 24]
;		push eax
;		mov eax, [ebx + 28]
;		push eax
;		mov eax, [ebx + 32]
;		push eax
;		mov eax, [ebx + 36]
;		push eax
;		mov eax, [ebx + 40]
;		push eax

;		pop edx
;		pop ecx
;		pop ebx
;		pop eax
		
		mov al, 0x20
		out 0x20, al

		;popad

	iret

	;-----------------------------------------------------------------------------------------------	

%define __key.shift.r__ 0x36
%define __key.shift.l__ 0x2a
%define __key.ctrl.r__  
%define __key.ctrl.l__
%define __key.alt.r__
%define __key.alt.l__
%define __key.numLock__  0x45
%define __key.maj__      0x3a
	
	%define __key.enabled.1.mask.lShift__ 00000001
	%define __key.enabled.1.mask.rShift__ 00000010
	%define __key.enabled.1.mask.maj__    00000100
	%define __key.enabled.1.mask.ctrl__   00001000
	%define __key.enabled.1.mask.alt__    00010000
	%define __key.enabled.1.mask.meta__   00100000
	%define __key.enabled.1.mask.home__   01000000
	%define __key.enabled.1.mask.cmd__    10000000

	key.enabled: dd 0x00000000
	spckey: db 0x00
	vr1: db 0x00
	
	irq.01:	
		pusha
		
		xor eax, eax
		xor ebx, ebx
		xor ecx, ecx
		xor edx, edx

		.standBuffer:
			in al, 0x64
			and al, 00000001
	
			cmp al, 0x01
			jne .standBuffer
			
		.readBuffer:
			in al, 0x60
			dec al

		.checking:
			cmp al, 0x80
			ja .return

		.matchChar:
			mov bl, [eax + charsmap]
			cmp bl, 0x00
			je .return
			
		.showChar:
			AfficherCaractere(bl)

		.return:
			;mov ax, [tdtr]
			;cmp ax, 0x0001
			;jne .eoi
			
			;AfficherCaractere('*')
			
		.eoi:
			popa

			mov al, 0x20
			out 0x20, al
		
		.iret:
			iret

	;-----------------------------------------------------------------------------------------------	
	
	isr.afficherTexte:
		xor eax, eax
		xor ebx, ebx
		
		AfficherCaractere(al)
		
		iret
;push word 0x0033
;push dword 0x00000000
;
;push dword 0x00000246
;
;push word 0x0023
;push dword beforewait
;
		
		;AfficherCaractere(' ')
		;mov eax, [esp]
		;mov ch, 0x02
		;call hex

		;AfficherCaractere(' ')
		;mov ax, [esp + 4]
		;mov ch, 0x01
		;call hex

		;AfficherCaractere(' ')
		;mov eax, [esp + 6]
		;mov ch, 0x02
		;call hex

		;AfficherCaractere(' ')
		;mov eax, [esp + 10]
		;mov ch, 0x02
		;call hex

		;AfficherCaractere(' ')
		;mov ax, [esp + 14]
		;mov ch, 0x01
		;call hex

		;------------------------;

		;AfficherCaractere(' ')
		;mov eax, [esp + 16]
		;mov ch, 0x02
		;call hex
		
		;AfficherCaractere(' ')
		;mov ax, [esp + 20]
		;mov ch, 0x01
		;call hex
		
		;AfficherCaractere(' ')
		;mov eax, [esp + 22]
		;mov ch, 0x02
		;call hex
		
		;AfficherCaractere(' ')
		;mov eax, [esp + 26]
		;mov ch, 0x02
		;call hex
				
		;AfficherCaractere(' ')
		;mov ax, [esp + 30]
		;mov ch, 0x01
		;call hex
		
		;pop dword eax
		;pop word  ax
		;pop dword eax
		;pop dword eax
		;pop word  ax
		
		;iretd
		
;	__key.specschars__:
;	
;		mov [spckey], byte 1
;		AfficherCaractere('%')
;		jmp irq.01.end
;
;	other:
;		intest2:
;		
;		in al, 0x64
;		and al, 00000001
;	
;		cmp al, 0x01
;		jne intest2
;			
;		in al, 0x60
;		
;		mov bl, [eax + charsmap]
;		AfficherCaractere(bl)
;
;		mov [spckey], byte 0x00
;
;		jmp irq.01.end
;
;	__key.maj.test__:
;		
;		xor [key.enabled], byte __key.enabled.1.mask.maj__
;		jmp irq.01.end
;
;keyboard_ctrl_send_cmd:
;
;	in al, 0x64
;	and al, 00000010
;	cmp al, 0x00
;	jne keyboard_ctrl_send_cmd
;	
;	mov al, ah
;	out 0x64, al
;
;ret
;
;keyboard_enc_send_cmd:
;
;	in al, 0x64
;	and al, 00000001
;	cmp al, 0x00
;	ja keyboard_enc_send_cmd
;	
;	mov al, ah
;	out 0x60, al
;
;ret
;
;
;		xor edx, edx
;		xor eax, eax
;		xor ebx, ebx
;		xor ecx, ecx
;		
;		;intest:
;		
;		in al, 0x64
;		and al, 00000001
;	
;		cmp al, 0x01
;		jne intest
;			
;		in al, 0x60
;		cmp al, 0xe0
;		je near __key.specschars__
;		dec al
;
;		cmp al, 0x80
;		jae brk_code
;			mov bl, [eax + charsmap]
;			cmp bl, 0
;			je nxts
;
;			AfficherCaractere(bl)
;			jmp irq.01.end
;		nxts:
;			mov ebx, 1
;			mov edx, 2
;			xchg ebx, eax
;			
;			mul edx
;
;			cmp bl, 0x00
;			cmove ecx, eax
;			
;			mul edx
;
;			cmp bl, 0x00
;			cmove ecx, eax
;			
;			mul edx
;
;			cmp bl, 0x00
;			cmove ecx, eax
;			
;			mul edx
;
;			cmp bl, 0x00
;			cmove ecx, eax
;			
;			mul edx
;
;			cmp bl, 0x00
;			cmove ecx, eax
;			
;			mul edx
;
;			cmp bl, 0x00
;			cmove ecx, eax
;			
;			mul edx
;
;			cmp bl, 0x00
;			cmove ecx, eax
;			
;			mul edx
;
;			cmp bl, 0x00
;			cmove ecx, eax
;			
;			
;			
;			xor [ key.enabled ], ecx
;		brk_code:
		
			



















;			shr bl, 4
;			and al, 0x0f
;			
;			cmp al, 0xa
;			jb ctne
;			
;			add al, 0x7
;						
;			ctne:
;			cmp bl, 0xa
;			jb ctne2
;			
;			add bl, 0x7
;						
;			ctne2:
			
;			add al, 0x30
;			add bl, 0x30
			
;			AfficherCaractere('0')
;			AfficherCaractere('x')
			
;			AfficherCaractere(bl)
;			AfficherCaractere(al)
;			AfficherCaractere(' ')


;			cmp al, 0xe0
;			cmove dx, ax
;			mov [vr1], dl
;
;			;cmp al, 0x80
;			;ja brakeCode
;
;			cmp [vr1], byte 0xe0
;			ja stdMakeCode
;				
;				dec al
;
;				mov bl, [eax + specsmap]
;				cmp bl, 0x00
;				je brakeCode
;				
;				AfficherCaractere(bl)
;				
;				mov [vr1], byte 0x00
;				
;				jmp brakeCode
;			stdMakeCode:
;			
;				dec al
;
;				mov bl, [eax + charsmap]
;				cmp bl, 0x00
;				je brakeCode
;				
;				AfficherCaractere(bl)
;			






			;mov al, 0x5c
;			mov bl, al
;
;			shr bl, 4
;			and al, 0x0f
;			
;			cmp al, 0xa
;			jb ctne
;			
;			add al, 0x7
;						
;			ctne:
;			cmp bl, 0xa
;			jb ctne2
;			
;			add bl, 0x7
;						
;			ctne2:
;			
;			add al, 0x30
;			add bl, 0x30
;			
;			AfficherCaractere('0')
;			AfficherCaractere('x')
;			
;			AfficherCaractere(bl)
;			AfficherCaractere(al)
;			AfficherCaractere(' ')
;
;			mov bl, [eax + chars]
;			AfficherCaractere(bl)
			



































;	irq_01:
;		inc byte [irq_01.count]
;			
;		AfficherTexte (irq_01.text)
;
;		cmp byte [irq_01.count], 100
;		jne irq_01.fin
;		mov byte [irq_01.count], 0x00
;
;		irq_01.fin:
;			mov al, 0x20
;			out 0x20, al
;	iret


;    IRQ_0_texte : db 'Interruption 0', CR, 0x00
;	IRQ_0 :
;
;		AfficherTexte ( IRQ_0_texte )
;
;		mov al, 0x20
;	    out 0x20, al
;
;		iret
;_valeur_x_ : db 0
;_valeur_y_ : db 1
;_m_afficherRect8_.ref   : dd 0xffffffff
;_m_afficherRect8_.debut : dd 0xffffffff
;_m_afficherRect8_ :
;
;	mov [ _m_afficherRect8_.ref ], dword MENU
;
;	xor EAX, EAX
;	xor EBX, EBX
;	xor ECX, ECX
;	xor EDX, EDX
;
;	mov EAX, [ _valeur_x_ ]
;
;
;	mov CL, LARGEUR_ECRAN
;
;	mul CL
;
;	add EAX, 0x000B8000
;
;	xor EDX, EDX
;
;	mov DH, 0x07
;
;	mov ECX, 70
;	mov BH, 10
;	mov BL, 0x00
;
;	add ECX, ECX
;	add ECX, EAX
;
;
;	_m_afficherRect8_.afficher :
;
;		mov [ EAX + 1 ], DH
;		add EAX, 2
;
;		cmp EAX, ECX
;		jb _m_afficherRect8_.afficher
;
;		sub EAX, 140
;
;		add EAX, 0xA0
;		add ECX, 0xA0
;
;
;
;		inc BL
;
;		cmp BL, BH
;
;		jne _m_afficherRect8_.afficher
;
;
;	ret
;

maine:
;call maine.start
;absolute 0x00030000
maine.start:
	.pause:
	;sti
	mov ax, cs
	;int 42
	;mov [ terminal.vCurseur ], dword 0x000B8b00

;	mov ax, 0x40
;	mov ds, ax
;	mov es, ax
;	mov fs, ax
;	mov gs, ax
	;mov [0xb80000], byte '/'
	;xor eax, eax
	;mov ebx, maine.text
	;int 42
	;AfficherTexte(maine.text)
	;mov [0xb8000], byte '4'
	
		hlt
		;jmp .pause
	jmp 0x08:beforewait
	
maine.text: db 'Ceci est le text de la fonction maine', CR, NUL
	
maine.end:
resb 100