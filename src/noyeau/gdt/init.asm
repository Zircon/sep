;---------------------------------------------------------------------------------------------------
; File name:        init.asm
; Version:          1.0.0.0
; Author:           Maxime Jeanson
; Copyright:        (c) 2010 by Sep System, all rights reserved.
;
; Documentations:   http://
;---------------------------------------------------------------------------------------------------

%ifndef __KERNEL_GDT_INIT_ASM__
%define __KERNEL_GDT_INIT_ASM__
	
	%include "inc/noyeau/descriptors/gdt.inc"
	
	gdt.init () ; Initialisation of the GDTR.
	
	gdt.add.desc (__GDT__,  1, 0x00000000, 0x0800, 0x0493) ; This is the descriptor of the GDT.
    
	gdt.add.desc (__IDT__,  2, 0x00000800, 0x0800, 0x0493) ; This is the descriptor of the IDT.
    
	gdt.add.desc (__LLDT__, 3, 0x00000800, 0x0800, 0x0493) ; This is the descriptor for the library
                                                           ; loading descriptor table (See the file
                                                           ; "src/kernel/library/kll.asm" for more
                                                           ; informations).
                                                      
	gdt.add.desc (__KBS__,  4, 0x00001800, 0x0800, 0x049B) ; This is the descriptor for the kernel
                                                           ; binnary and it's system management.
    
    
	
%endif ; __KERNEL_GDT_INIT_ASM__