; Hardware abstraction layout.
;	%define CR 0x00
		
	%define __CPU_UNKNOWN__ 0x00
	%define __CPU_INTEL__   0x01
	%define __CPU_AMD__     0x02

	[section .bss align=1]
	
	; Current
	__vendor_id__ : resb 1
	__model_id__  : resb 1
	__family_id__ : resb 2
	__type_id__   : resb 1
	__revision__  : resb 1
	__f_flags__   : resb 8
	__psn__       : resb 12
	
	__brand_id__  : resb 1
	__chunks__    : resb 1
	__count__     : resb 1
	__apic_id__   : resb 1
	
	__config__    : resb 16
	

	[section .text]

	__Error_Processor_Check_Fails__ : db 'Error : Processor Check fails : This Processor is not support!', CR, 0x00

	; Intel
	__intel_vendor_id__ : db 'GenuineIntel', 0x00
	
	; AMD
	__amd_vendor_id__   : db 'AuthenticAMD', 0x00
		
	; Fonction
	__Processor_Check__:	
		xor eax, eax
		
		cpuid
				
		__Intel_Check__ :
			cmp ebx, [ __intel_vendor_id__ ]
			jne __AMD_Check__
			
			cmp edx, [ __intel_vendor_id__ + 4 ]
			jne __AMD_Check__
			
			cmp ecx, [ __intel_vendor_id__ + 8 ]
			jne __AMD_Check__
		
			mov byte [ __vendor_id__ ], byte __CPU_INTEL__
			
			jmp __Intel_Setup__
			
		__AMD_Check__ :
			cmp ebx, [ __amd_vendor_id__ ]
			jne __Processor_Check_Fails__
			
			cmp edx, [ __amd_vendor_id__ + 4 ]
			jne __Processor_Check_Fails__
			
			cmp ecx, [ __amd_vendor_id__ + 8 ]
			jne __Processor_Check_Fails__

			mov byte [ __vendor_id__ ], byte __CPU_INTEL__
			
			jmp __AMD_Setup__
			
		__Processor_Check_Fails__ :
			AfficherTexte(__Error_Processor_Check_Fails__)
		
		__Processor_Check_End__ :
			AfficherCaractere('C')
			
			mov byte eax, [__model_id__]
			mov ch, 0x00
			call hex
			AfficherCaractere(' ')
			
			mov byte eax, [__family_id__]
			mov ch, 0x01
			call hex
			AfficherCaractere(' ')
			
			mov byte eax, [__type_id__]
			mov ch, 0x00
			call hex
			AfficherCaractere(' ')
			
			mov byte eax, [__revision__]
			mov ch, 0x00
			call hex
			AfficherCaractere(CR)


			mov byte eax, [__f_flags__]
			mov ch, 0x02
			call hex
			AfficherCaractere(' ')

			mov byte eax, [__f_flags__ + 4]
			mov ch, 0x02
			call hex
			AfficherCaractere(' ')
			
			mov byte eax, [__brand_id__]
			mov ch, 0x02
			call hex
			AfficherCaractere(' ')
			
			ret
			
			
	__Intel_Setup__ :
		AfficherTexte(__intel_vendor_id__)
	
		cmp eax, 0x00000000
		jna near __Intel_Ext_Setup__
		
		__Processor_Signature__ :
			push eax
			
			mov eax, 0x00000001
			cpuid
			
			mov dword [__psn__],            eax
			mov dword [__brand_id__],       ebx
			mov dword [__f_flags__ + 0x04], ecx
			mov dword [__f_flags__],        edx
			
			xor ecx, ecx
			mov ebx, eax
			
			; Model identification
			and eax, 0x000F00F0
			shr eax, 4
			mov cl, al
			add cl, ah
			mov byte [__model_id__], cl
			
			xor ecx, ecx
			mov eax, ebx
			
			; Family identification
			and eax, 0x0FF00F00
			shr eax, 8
			mov cl, al
			shr eax, 8
			add cx, ax
			mov word [__family_id__], cx

			xor ecx, ecx
			mov eax, ebx			
			
			; Type identification
			and eax, 0x0000E000
			shr eax, 13
			mov byte [__type_id__], al
			
			xor ecx, ecx
			mov eax, ebx			

			; Revision identification
			and eax, 0x0000000F
			mov byte [__revision__], al
			
			pop eax
		
				
		cmp eax, 0x00000001
		jna __Intel_Ext_Setup__

		__Processor_Configuration__ :
			push eax
			
			mov eax, 0x00000002
			cpuid
			
			
			pop eax
			
	__Intel_Ext_Setup__ :
			jmp __Processor_Check_End__
		
		__AMD_Setup__ :