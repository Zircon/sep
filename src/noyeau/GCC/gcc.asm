;===================================================================================================
; Graphics controler configuration
;===================================================================================================
%ifndef __GRAPHICS_CONTROLER_CONFIGURATION__
%define __GRAPHICS_CONTROLER_CONFIGURATION__

	;===============================================================================================
	; Graphics controler identification
	;===============================================================================================
	__graphics_controler_identification__:
		
		.cirrus_logic
			call __cirrus_logic_identification__
		ret
	
	
	
%endif ; __GRAPHICS_CONTROLER_CONFIGURATION__