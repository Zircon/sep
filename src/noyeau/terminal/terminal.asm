%define ___copmilataion_system_base___ unix

%ifidn ___copmilataion_system_base___, unix
	%include "inc/noyeau/terminal/terminal.inc"
%elifidn ___copmilataion_system_base___, win32
	%include "E:\Projet Sep\Noyeau\Terminal\Terminal.inc"
%else
	%error "Compilation impossible, le systeme qui permet de compiler n'est pas defini."
%endif


%ifndef TERMINAL_ASM
%define TERMINAL_ASM

	%ifndef TERMINAL_ASM_VARIABLES
	%define TERMINAL_ASM_VARIABLES
	
;	    terminal.vAttribut   : db 00000010b  ; Attributs du caract�re.
	    terminal.vCurseur    : dd 0x000B8000 ; Position du curseur.
;		terminal.vBaseEcran  : dd 0x10000000 ; Position de la base de l'�cran.
;		terminal.vFinEcran   : dd 0x10000000 ; Position de la fin de l'�cran.
	    terminal.vSauvegarde : dd 0x10000000 ; Position du curseur de sauvegarde.
	
	%endif ; TERMINAL_ASM_VARIABLES
	
	%ifndef TERMINAL_ASM_DEFINITIONS_PRIVEES
	%define TERMINAL_ASM_DEFINITIONS_PRIVEES
		
		%define chargerAttribut( reg8 )        mov reg8,  byte  [ terminal.vAttribut ]
		%define chargerCurseur( reg32 )        mov reg32, dword [ terminal.vCurseur ]
		%define chargerSauvegarde( reg32 )     mov reg32, dword [ terminal.vSauvegarde ]
		
		%define sauvegarderAttribut( reg8 )    mov byte  [ terminal.vAttribut ],   reg8
		%define sauvegarderCurseur( reg32 )    mov dword [ terminal.vCurseur ],    reg32
		%define sauvegarderSauvegarde( reg32 ) mov dword [ terminal.vSauvegarde ], reg32
	
	%endif ; TERMINAL_ASM_DEFINITIONS_PRIVEES
		
    %ifndef TERMINAL_ASM_AFFICHERCARACTERE
    %define TERMINAL_ASM_AFFICHERCARACTERE
        ;-------------------------------------------------------------------------------------------
		; MARK: Afficher caract�re
        ; Cette fonction affiche un simple caract�re dans le terminal. Pour se faire, elle
        ; n�ss�ssite un caract�re huit bits situer dans le registre AL. Par la suite, il �ffectue
        ; les opp�rations suivantes :
        ; - V�rifications : Il v�rifie si le caract�re entr�e en param�tre est un caract�re sp�cial
        ;   exemple : TAB, CR, VT, LF, etc...
        ; - Affichage : Il affiche le caract�re en appelant la sous fonction approprier.
        ; - Mise � jour du curseur VGA : Il �ffectue une mise � jour de sa position dans le
        ;   terminal.
        ;-------------------------------------------------------------------------------------------
        terminal.afficherCaractere :
            ;---------------------------------------------------------------------------------------
            ; Cette fonction �ffectue des v�rifications afin de savoir quelle caract�re afficher
            ; dans le terminal. Pour se faire, elle �ffectue les opp�rations suivantes :
            ; - V�rifivations :
            ;    - Compare la valeur du registre AL avec un caract�re de tabulation. S' il en est
            ;      une, nous �ffectuons un saut � la fonction qui g�re ce caract�re.
            ;    - Compare la valeur du registre AL avec un caract�re de retour � la ligne. S' il
            ;      en est un, nous �ffectuons un saut � la fonction qui g�re ce caract�re.
            ;    - Compare la valeur du registre AL avec un caract�re de tabulation vertical. S' il
            ;      en est une, nous �ffectuons un saut � la fonction qui g�re ce caract�re.
            ;    - Compare la valeur du registre AL avec un caract�re de retour du chariot. S' il
            ;      en est un, nous �ffectuons un saut � la fonction qui g�re ce caract�re.
            ;    - Sinon, nous affichons le caract�re avec la fonction qui affiche un simple
            ;      caract�re.
            ;---------------------------------------------------------------------------------------
            terminal.afficherCaractere.verifications :

                ;-----------------------------------------------------------------------------------
                ; V�rifivations
                ;-----------------------------------------------------------------------------------

                cmp AL, TAB                            ; Compare le registre AL avec TAB.
                jz terminal.afficherCaractere.TAB      ; affiche une TAB s'il sont �gale.

                cmp AL, CR                             ; Compare le registre AL avec CR.       
                jz NEAR terminal.afficherCaractere.CR       ; affiche un CR s'il sont �gale.

                cmp AL, VT                             ; Compare le registre AL avec VT.
                jz NEAR terminal.afficherCaractere.VT  ; affiche un VT s'il sont �gale.

                cmp AL, LF                             ; Compare le registre AL avec LF.
                jz NEAR terminal.afficherCaractere.LF  ; affiche un LF s'il sont �gale.

                ; Sinon il s'agit d'un caract�re alors l'on affiche celui-ci.
			
            ;---------------------------------------------------------------------------------------
            ; Cette fonction affiche un caract�re dans le terminal. Pour se faire, elle �ffectue
            ; les opp�rations suivantes :
            ; - Chargement des variables :
            ;    - Charger l'attribut du caract�re dans le registre AH afin d'�tre utiliser par le 
            ;      reste de la fonction.
            ;    - Charger la position du curseur dans le registre EBX afin d'obtenir la position 
            ;      d'�criture du caract�re suivant dans le terminal.
            ;    - Charger la position du curseur de sauvegarde dans le registre ECX afin de 
            ;      pouvoir sauvegarder une copie des �l�ments �crits dans le terminal pour un 
            ;      d�filement haut ou bas, �ventuel, du terminal.
            ; - Affichage :
            ;    - Copie du caract�re et son attribut dans le terminal � la position du curseur.
            ; - Sauvegarde du texte :
            ;    - Copie le caract�re � la position du registre ECX afin de sauvegarder le
            ;      caract�re pour un �ventuel d�filement, haut ou bas, du terminal.
            ;    - Nous incr�mentons, ensuite, la position du curseur de sauvegarde.
            ; - Sauvegarde des variables :
            ;    - Sauvegarde la position du curseur situ� dans le registre EBX.
            ;    - Sauvegarde la position du curseur de sauvegarde situ� dans le registre ECX.
            ; - Retour :
            ;    - Saut vers la petite fonction de mise � jour du curseur vga.
            ;---------------------------------------------------------------------------------------
            terminal.afficherCaractere.caractere :
                ;-----------------------------------------------------------------------------------
                ; Chargement des variables
                ;-----------------------------------------------------------------------------------

;                chargerAttribut   ( AH )  ; Charge l'attribut du caract�re dans le registre AH.
                chargerCurseur    ( EBX ) ; Charge la position du curseur dans le registre EBX.
;                chargerSauvegarde ( ECX ) ; Charde la position du curseur de sauvegarde dans ECX.

                ;-----------------------------------------------------------------------------------
                ; Affichage
                ;-----------------------------------------------------------------------------------

                mov byte [ EBX ], AL ; Copie le caract�re et son attribut dans le terminal.
                add EBX, 2           ; Ajoute 2 la position du curseur.

                ;-----------------------------------------------------------------------------------
                ; Sauvegarde du texte
                ;-----------------------------------------------------------------------------------

;                mov byte [ ECX ], AL ; Ajoute le caract�re � la sauvegarde.
;                inc ECX              ; Incr�mente la position du curseur de savegarde.

                ;-----------------------------------------------------------------------------------
                ; Sauvegarde des variables
                ;-----------------------------------------------------------------------------------

                sauvegarderCurseur    ( EBX ) ; Sauvegarde la position du curseur.
;                sauvegarderSauvegarde ( ECX ) ; Sauvegarde la position du curseur de sauvegarde.

                ;-----------------------------------------------------------------------------------
                ; Retour
                ;-----------------------------------------------------------------------------------

                jmp terminal.afficherCurseur ; Mise � jour du curseur.
			
            ;---------------------------------------------------------------------------------------
            ; Cette fonction affiche une tabulation dans le terminal. Pour se faire, elle �ffectue
            ; les opp�rations suivantes :
            ; - Chargement des variables :
            ;    - Charger la position du curseur dans le registre EBX afin d'obtenir la position 
            ;      d'�criture du caract�re suivant dans le terminal.
            ;    - Charger la position du curseur de sauvegarde dans le registre ECX afin de 
            ;      pouvoir sauvegarder une copie des �l�ments �crits dans le terminal pour un 
            ;      d�filement haut ou bas, �ventuel, du terminal.
            ;    - Charger l'attribut du caract�re dans le registre DH afin d'�tre utiliser par le 
            ;      reste de la fonction.
            ; - Affichage :
            ;    - Initialise EAX pour la suite.
            ;    - On copie l'attribut du caract�re de DH � AH afin de colorer le fond.
            ;    - On d�cale EAX de 16 bits vers la gauche afin d'avoir la m�me valeur dans la
            ;      partie haute et basse de EAX.
            ;    - On copie l'attribut du caract�re de DH � AH afin de colorer le fond.
            ;    - Copie du registre EAX dans le terminal � la position du curseur.
            ;    - Ajoute 4 � la position du curseur.
            ;    - Copie du registre EAX dans le terminal � la position du curseur.
            ;    - Ajoute 4 � la position du curseur.
            ; - Sauvegarde du texte :
            ;    - Copie un caract�re de tabulation � la position du registre ECX afin de
            ;      sauvegarder le caract�re pour un �ventuel d�filement, haut ou bas, du terminal.
            ;    - Nous incr�mentons, ensuite, la position du curseur de sauvegarde.
            ; - Sauvegarde des variables :
            ;    - Sauvegarde la position du curseur situ� dans le registre EBX.
            ;    - Sauvegarde la position du curseur de sauvegarde situ� dans le registre ECX.
            ; - Retour :
            ;    - Saut vers la petite fonction de mise � jour du curseur vga.
            ;---------------------------------------------------------------------------------------
            terminal.afficherCaractere.TAB :
                ;-----------------------------------------------------------------------------------
                ; Chargement des variables
                ;-----------------------------------------------------------------------------------

                chargerCurseur    ( EBX ) ; Charge la position du curseur dans le registre EBX.
;                chargerSauvegarde ( ECX ) ; Charde la position du curseur de sauvegarde dans ECX.
;                chargerAttribut   ( DL )  ; Charge l'attribut du caract�re dans le registre DL.

                ;-----------------------------------------------------------------------------------
                ; Affichage
                ;-----------------------------------------------------------------------------------
                
                add EBX, 8             ; Ajoute 4 � la position du curseur.
                
                ;-----------------------------------------------------------------------------------
                ; Sauvegarde du texte
                ;-----------------------------------------------------------------------------------
                                
;                mov byte [ ECX ], TAB ; Ajoute une tabulation � la sauvegarde.
;                inc ECX               ; Incr�mente la position du curseur de savegarde.

                ;-----------------------------------------------------------------------------------
                ; Sauvegarde des variables
                ;-----------------------------------------------------------------------------------

                sauvegarderCurseur    ( EBX ) ; Sauvegarde la position du curseur.
;                sauvegarderSauvegarde ( ECX ) ; Sauvegarde la position du curseur de sauvegarde.
                
                ;-----------------------------------------------------------------------------------
                ; Retour
                ;-----------------------------------------------------------------------------------

                jmp terminal.afficherCurseur ; Mise � jour du curseur.

            ;---------------------------------------------------------------------------------------
            ; Cette fonction affiche un saut � la ligne dans le terminal. Pour se faire, elle 
            ; �ffectue les opp�rations suivantes :
            ; - Chargement des variables :
            ;    - Charger la position du curseur dans le registre EAX afin d'obtenir la position 
            ;      d'�criture du caract�re suivant dans le terminal.
            ;    - Charger la position du curseur de sauvegarde dans le registre EBX afin de 
            ;      pouvoir sauvegarder une copie des �l�ments �crits dans le terminal pour un 
            ;      d�filement haut ou bas, �ventuel, du terminal.
            ; - Affichage :
            ;    - Initialisation du registre EDX.
            ;    - Soustrait 0x000B8000 du registre EAX afin d'obtenir seulement le d�placement
            ;      depui le d�but du terminal.
            ;    - Copie la largeur de l'�cran dans le registre ECX.
            ;    - Divise la position du curseur par la largeur de l'�cran afin de savoir sur
            ;      quelle ligne est positionner le curseur.
            ;    - Incr�mente la position du curseur sur ligne suivante.
            ;    - Multiplie la position du curseur par la largeur de l'�cran afin de savoir quel
            ;      est le d�placement depui le d�but du terminal.
            ; - Sauvegarde du texte :
            ;    - Copie un caract�re de retour � la ligne � la position du registre EBX afin de
            ;      sauvegarder le caract�re pour un �ventuel d�filement, haut ou bas, du terminal.
            ;    - Nous incr�mentons, ensuite, la position du curseur de sauvegarde.
            ; - Sauvegarde des variables :
            ;    - Sauvegarde la position du curseur situ� dans le registre EAX.
            ;    - Sauvegarde la position du curseur de sauvegarde situ� dans le registre EBX.
            ; - Retour :
            ;    - Saut vers la petite fonction de mise � jour du curseur vga.
            ;---------------------------------------------------------------------------------------
            terminal.afficherCaractere.CR :
                ;-----------------------------------------------------------------------------------
                ; Chargement des variables
                ;-----------------------------------------------------------------------------------

                chargerCurseur    ( EAX ) ; Charge la position du curseur dans EAX.
;                chargerSauvegarde ( EBX ) ; Charde la position du curseur de sauvegarde dans EBX.

                ;-----------------------------------------------------------------------------------
                ; Affichage
                ;-----------------------------------------------------------------------------------

                xor EDX, EDX           ; Initialisation du registre EDX.

                sub EAX, 0x000B8000    ; Soustrait 0x000B8000 au curseur.

                mov ECX, LARGEUR_ECRAN ; Copie la largeur de l'�cran dans le registre ECX.
                
                div ECX                ; Divise la position du curseur par la largeur de l'�cran.
                inc EAX                ; Incr�mente la position du curseur sur ligne suivante.
                mul ECX                ; Multiplie la position du curseur par la largeur de l'�cran.
                
                add EAX, 0x000B8000    ; Ajoute 0x000B8000 au curseur.

                ;-----------------------------------------------------------------------------------
                ; Sauvegarde du texte
                ;-----------------------------------------------------------------------------------

;                mov byte [ EBX ], LF ; Ajoute un retour � la ligne � la sauvegarde.
;                inc EBX              ; Incr�mente la position du curseur de savegarde.

                ;-----------------------------------------------------------------------------------
                ; Sauvegarde des variables
                ;-----------------------------------------------------------------------------------

                sauvegarderCurseur    ( EAX ) ; Sauvegarde la position du curseur.
;                sauvegarderSauvegarde ( EBX ) ; Sauvegarde la position du curseur de sauvegarde.

                ;-----------------------------------------------------------------------------------
                ; Retour
                ;-----------------------------------------------------------------------------------

                jmp terminal.afficherCurseur ; Mise � jour du curseur.

            ;---------------------------------------------------------------------------------------
            ; Cette fonction affiche une tabulation vertical dans le terminal. Pour se faire, elle 
            ; �ffectue les opp�rations suivantes :
            ; - Chargement des variables :
            ;    - Charger la position du curseur dans le registre EAX afin d'obtenir la position 
            ;      d'�criture du caract�re suivant dans le terminal.
            ;    - Charger la position du curseur de sauvegarde dans le registre EBX afin de 
            ;      pouvoir sauvegarder une copie des �l�ments �crits dans le terminal pour un 
            ;      d�filement haut ou bas, �ventuel, du terminal.
            ; - Affichage :
            ;    - Ajoute la valeur 0x9E au registre EAX afin de positionner le curseur sur la
            ;      ligne suivante.
            ; - Sauvegarde du texte :
            ;    - Copie un caract�re de tabulation vertical � la position du registre EBX afin de
            ;      sauvegarder le caract�re pour un �ventuel d�filement, haut ou bas, du terminal.
            ;    - Nous incr�mentons, ensuite, la position du curseur de sauvegarde.
            ; - Sauvegarde des variables :
            ;    - Sauvegarde la position du curseur situ� dans le registre EAX.
            ;    - Sauvegarde la position du curseur de sauvegarde situ� dans le registre EBX.
            ; - Retour :
            ;    - Saut vers la petite fonction de mise � jour du curseur vga.
            ;---------------------------------------------------------------------------------------
            terminal.afficherCaractere.VT :
                ;-----------------------------------------------------------------------------------
                ; Chargement des variables
                ;-----------------------------------------------------------------------------------

                chargerCurseur    ( EAX ) ; Charge la position du curseur dans EAX.
;                chargerSauvegarde ( EBX ) ; Charde la position du curseur de sauvegarde dans EBX.

                ;-----------------------------------------------------------------------------------
                ; Affichage
                ;-----------------------------------------------------------------------------------

                add EAX, 0x9E ; Ajoute la valeur 0x9E au registre EAX.
                
                ;-----------------------------------------------------------------------------------
                ; Sauvegarde du texte
                ;-----------------------------------------------------------------------------------

;                mov byte [ EBX ], VT ; Ajoute une tabulation vertical � la sauvegarde.
;                inc EBX              ; Incr�mente la position du curseur de savegarde.

                ;-----------------------------------------------------------------------------------
                ; Sauvegarde des variables
                ;-----------------------------------------------------------------------------------

                sauvegarderCurseur    ( EAX ) ; Sauvegarde la position du curseur.
;                sauvegarderSauvegarde ( EBX ) ; Sauvegarde la position du curseur de sauvegarde.

                ;-----------------------------------------------------------------------------------
                ; Retour
                ;-----------------------------------------------------------------------------------

                jmp terminal.afficherCurseur ; Mise � jour du curseur.
			
            ;---------------------------------------------------------------------------------------
            ; Cette fonction affiche un retour du chariot dans le terminal. Pour se faire, elle 
            ; �ffectue les opp�rations suivantes :
            ; - Chargement des variables :
            ;    - Charger la position du curseur dans le registre EAX afin d'obtenir la position 
            ;      d'�criture du caract�re suivant dans le terminal.
            ;    - Charger la position du curseur de sauvegarde dans le registre EBX afin de 
            ;      pouvoir sauvegarder une copie des �l�ments �crits dans le terminal pour un 
            ;      d�filement haut ou bas, �ventuel, du terminal.
            ; - Affichage :
            ;    - Initialisation du registre EDX.
            ;    - Soustrait 0x000B8000 du registre EAX afin d'obtenir seulement le d�placement
            ;      depui le d�but du terminal.
            ;    - Copie la largeur de l'�cran dans le registre ECX.
            ;    - Divise la position du curseur par la largeur de l'�cran afin de savoir sur
            ;      quelle ligne est positionner le curseur.
            ;    - Multiplie la position du curseur par la largeur de l'�cran afin de savoir quel
            ;      est le d�placement depui le d�but du terminal.
            ; - Sauvegarde du texte :
            ;    - Copie un caract�re de retour du chariot � la position du registre EBX afin de
            ;      sauvegarder le caract�re pour un �ventuel d�filement, haut ou bas, du terminal.
            ;    - Nous incr�mentons, ensuite, la position du curseur de sauvegarde.
            ; - Sauvegarde des variables :
            ;    - Sauvegarde la position du curseur situ� dans le registre EAX.
            ;    - Sauvegarde la position du curseur de sauvegarde situ� dans le registre EBX.
            ; - Retour :
            ;    - Saut vers la petite fonction de mise � jour du curseur vga.
            ;---------------------------------------------------------------------------------------
            terminal.afficherCaractere.LF :
                ;-----------------------------------------------------------------------------------
                ; Chargement des variables
                ;-----------------------------------------------------------------------------------

                chargerCurseur    ( EAX ) ; Charge la position du curseur dans EAX.
;                chargerSauvegarde ( EBX ) ; Charde la position du curseur de sauvegarde dans ECX.

                ;-----------------------------------------------------------------------------------
                ; Affichage
                ;-----------------------------------------------------------------------------------

                xor EDX, EDX           ; Initialisation du registre EDX.

                sub EAX, 0x000B8000    ; Soustrait 0x000B8000 au curseur.

                mov ECX, LARGEUR_ECRAN ; Copie la largeur de l'�cran dans le registre ECX.
                
                div ECX                ; Divise la position du curseur par la largeur de l'�cran.
                mul ECX                ; Multiplie la position du curseur par la largeur de l'�cran.
                
                add EAX, 0x000B8000    ; Ajoute 0x000B8000 au curseur.

                ;-----------------------------------------------------------------------------------
                ; Sauvegarde du texte
                ;-----------------------------------------------------------------------------------

;                mov byte [ EBX ], CR ; Ajoute un retour � la ligne � la sauvegarde.
;                inc EBX              ; Incr�mente la position du curseur de savegarde.

                ;-----------------------------------------------------------------------------------
                ; Sauvegarde des variables
                ;-----------------------------------------------------------------------------------

                sauvegarderCurseur    ( EAX ) ; Sauvegarde la position du curseur.
;                sauvegarderSauvegarde ( EBX ) ; Sauvegarde la position du curseur de sauvegarde.

                ;-----------------------------------------------------------------------------------
                ; Retour
                ;-----------------------------------------------------------------------------------

                jmp terminal.afficherCurseur ; Mise � jour du curseur.

    %endif ; TERMINAL_ASM_AFFICHERCARACTERE


    %ifndef TERMINAL_ASM_AFFICHERTEXTE
    %define TERMINAL_ASM_AFFICHERTEXTE
        ;-------------------------------------------------------------------------------------------
		; MARK: Afficher texte
        ; Cette fonction affiche une chaine de texte dans le terminal. Pour se faire, elle
        ; n�ss�ssite l'adresse d'une chaine de texte situ� dans le registre ESI. Par la suite, il
        ; �ffectue les opp�rations suivantes :
        ; - V�rifications : Il charge les caract�res un � un et les v�rifient pour les affichers.
        ; - Affichage : Il affiche les caract�res en appelant la fonction appropri�e.
        ; - Finalisation : Il sauvegarde un caract�re nulle dans la sauvegarde afin d'indiquer la
        ;   fin de la chaine de texte.
        ; - Mise � jour du curseur VGA : Il �ffectue une mise � jour de sa position dans le
        ;   terminal.
        ;-------------------------------------------------------------------------------------------
        terminal.afficherTexte :
						
            ;---------------------------------------------------------------------------------------
            ; Cette fonction �ffectue des v�rifications afin de savoir quelle caract�re afficher
            ; dans le terminal. Pour se faire, elle �ffectue les opp�rations suivantes :
            ; - Initialisations :
            ;    - Copie le caract�re situ� � l'addresse ESI.
            ;    - Incr�mente la position du curseur de lecture de la chaine de texte ESI.
            ; - V�rifivations :
            ;    - Compare la valeur du registre AL avec un caract�re nulle. S'il en est un, nous
            ;      �ffectuons un saut vers la fonction de mise � jour du curseur VGA.
            ;    - Compare la valeur du registre AL avec un caract�re de mise � jour de l'attribut
            ;      du texte. S'il en est un, nous �ffectuons un saut � la fonction qui g�re cette
            ;      fonction.
            ;    - Compare la valeur du registre AL avec un caract�re de tabulation. S' il en est
            ;      une, nous �ffectuons un saut � la fonction qui g�re ce caract�re.
            ;    - Compare la valeur du registre AL avec un caract�re de retour � la ligne. S' il
            ;      en est un, nous �ffectuons un saut � la fonction qui g�re ce caract�re.
            ;    - Compare la valeur du registre AL avec un caract�re de tabulation vertical. S' il
            ;      en est une, nous �ffectuons un saut � la fonction qui g�re ce caract�re.
            ;    - Compare la valeur du registre AL avec un caract�re de retour du chariot. S' il
            ;      en est un, nous �ffectuons un saut � la fonction qui g�re ce caract�re.
            ;    - Sinon, nous affichons le caract�re avec la fonction qui affiche un simple
            ;      caract�re.
            ;---------------------------------------------------------------------------------------
            terminal.afficherTexte.verifications :
			
                ;-----------------------------------------------------------------------------------
                ; Initialisations
                ;-----------------------------------------------------------------------------------
                
                mov AL, byte [ ESI ] ; Copie le caract�re situ� � l'addresse ESI dans AL.
                inc ESI              ; Incr�mente la position du curseur de lecture.

                ;-----------------------------------------------------------------------------------
                ; V�rifications
                ;-----------------------------------------------------------------------------------
 
                cmp AL, NUL                       ; Compare le registre AL avec NUL.
                jz NEAR terminal.afficherCurseur  ; Met � jour le curseur VGA s'il sont �gale.

;                cmp AL, SOH                       ; Compare le registre AL avec SOH.
;                jz terminal.afficherTexte.SOH     ; affiche un SOH s'il sont �gale.

                cmp AL, TAB                       ; Compare le registre AL avec TAB.
                jz terminal.afficherTexte.TAB	  ; affiche un TAB s'il sont �gale.

                cmp AL, CR                        ; Compare le registre AL avec CR.
                jz NEAR terminal.afficherTexte.CR ; affiche un CR s'il sont �gale.

                cmp AL, VT                        ; Compare le registre AL avec VT.
                jz NEAR terminal.afficherTexte.VT ; affiche un VT s'il sont �gale.

                cmp AL, LF                        ; Compare le registre AL avec LF.
                jz NEAR terminal.afficherTexte.LF ; affiche un LF s'il sont �gale.

                ; Sinon il s'agit d'un caract�re alors l'on affiche celui-ci.
				
            ;---------------------------------------------------------------------------------------
            ; Cette fonction affiche un caract�re dans le terminal. Pour se faire, elle �ffectue
            ; les opp�rations suivantes :
            ; - Chargement des variables :
            ;    - Charger l'attribut du caract�re dans le registre AH afin d'�tre utiliser par le 
            ;      reste de la fonction.
            ;    - Charger la position du curseur dans le registre EBX afin d'obtenir la position 
            ;      d'�criture du caract�re suivant dans le terminal.
            ;    - Charger la position du curseur de sauvegarde dans le registre ECX afin de 
            ;      pouvoir sauvegarder une copie des �l�ments �crits dans le terminal pour un 
            ;      d�filement haut ou bas, �ventuel, du terminal.
            ; - Affichage :
            ;    - Copie du caract�re et son attribut dans le terminal � la position du curseur.
            ; - Sauvegarde du texte :
            ;    - Copie le caract�re � la position du registre ECX afin de sauvegarder le
            ;      caract�re pour un �ventuel d�filement, haut ou bas, du terminal.
            ;    - Nous incr�mentons, ensuite, la position du curseur de sauvegarde.
            ; - Sauvegarde des variables :
            ;    - Sauvegarde la position du curseur situ� dans le registre EBX.
            ;    - Sauvegarde la position du curseur de sauvegarde situ� dans le registre ECX.
            ; - Retour :
            ;    - Retourne � la fonction qui v�rifie le caract�re suivant.
            ;---------------------------------------------------------------------------------------
            terminal.afficherTexte.caractere :
			
                ;-----------------------------------------------------------------------------------
                ; Chargement des variables
                ;-----------------------------------------------------------------------------------

;                chargerAttribut   ( AH )  ; Charge l'attribut du caract�re dans le registre AH.
                chargerCurseur    ( EBX ) ; Charge la position du curseur dans le registre EBX.
;                chargerSauvegarde ( ECX ) ; Charde la position du curseur de sauvegarde dans ECX.

                ;-----------------------------------------------------------------------------------
                ; Affichage
                ;-----------------------------------------------------------------------------------

                mov byte [ EBX ], AL ; Copie le caract�re et son attribut dans le terminal.
                add EBX, 2           ; Ajoute 2 la position du curseur.

                ;-----------------------------------------------------------------------------------
                ; Sauvegarde du texte
                ;-----------------------------------------------------------------------------------

;                mov byte [ ECX ], AL ; Ajoute le caract�re � la sauvegarde.
;                inc ECX              ; Incr�mente la position du curseur de savegarde.

                ;-----------------------------------------------------------------------------------
                ; Sauvegarde des variables
                ;-----------------------------------------------------------------------------------

                sauvegarderCurseur    ( EBX ) ; Sauvegarde la position du curseur.
;                sauvegarderSauvegarde ( ECX ) ; Sauvegarde la position du curseur de sauvegarde.

                ;-----------------------------------------------------------------------------------
                ; Retour
                ;-----------------------------------------------------------------------------------

                jmp terminal.afficherTexte.verifications ; V�rifie le caract�re suivant.
			
            ;---------------------------------------------------------------------------------------
            ; Cette fonction modifie la valeur de l'attribut des caract�res suivants dans le
            ; terminal. Pour ce faire, elle �ffectue les opp�rations suivantes :
            ; - Mise � jour :
            ;    - Lit le caract�re suivant qui repr�sente la nouvelle valeur de l'attribut.
			;    - Incr�mente la position du curseur de lecture de la chaine de texte ESI.
            ; - Sauvegarde du texte :
            ;    - Sauvegarde un caract�re SOH dans la sauvegarde.
			;    - Incr�mente la position du curseur de sauvegarde.
            ;    - Sauvegarde de l'attribut dans la sauvegarde.
			;    - Incr�mente la position du curseur de sauvegarde.
            ; - Sauvegarde des variables :
            ;    - Sauvegarde de l'attribut.
			;    - Sauvegarde de la position du curseur de sauvegarde.
            ; - Retour :
            ;    - Retourne � la fonction qui v�rifie le caract�re suivant.
            ;---------------------------------------------------------------------------------------
;			terminal.afficherTexte.SOH :
;			block
;			
                ;-----------------------------------------------------------------------------------
                ; Mise � jour
                ;-----------------------------------------------------------------------------------
				
;                mov AH, byte [ ESI ] ; Copie le caract�re situ� � l'addresse ESI dans AH.
;                inc ESI              ; Incr�mente la position du curseur de lecture.
								
                ;-----------------------------------------------------------------------------------
                ; Sauvegarde du texte
                ;-----------------------------------------------------------------------------------

;				mov byte [ ECX ], SOH ; Sauvegarde un caract�ere SOH.
;				inc ECX				  ; Incr�mente la position du curseur de sauvegarde.
;				mov byte [ ECX ], AH  ; Sauvegarde de l'attribut.
;				inc ECX				  ; Incr�mente la position du curseur de sauvegarde.

                ;-----------------------------------------------------------------------------------
                ; Sauvegarde des variables
                ;-----------------------------------------------------------------------------------

;				sauvegarderAttribut   ( AH )
;				sauvegarderSauvegarde ( ECX )

                ;-----------------------------------------------------------------------------------
                ; Retour
                ;-----------------------------------------------------------------------------------

;                jmp terminal.afficherTexte.verifications ; V�rifie le caract�re suivant.
			
;			endblock
				
            ;---------------------------------------------------------------------------------------
            ; Cette fonction affiche une tabulation dans le terminal. Pour se faire, elle �ffectue
            ; les opp�rations suivantes :
            ; - Chargement des variables :
            ;    - Charger la position du curseur dans le registre EBX afin d'obtenir la position 
            ;      d'�criture du caract�re suivant dans le terminal.
            ;    - Charger la position du curseur de sauvegarde dans le registre ECX afin de 
            ;      pouvoir sauvegarder une copie des �l�ments �crits dans le terminal pour un 
            ;      d�filement haut ou bas, �ventuel, du terminal.
            ;    - Charger l'attribut du caract�re dans le registre DH afin d'�tre utiliser par le 
            ;      reste de la fonction.
            ; - Affichage :
            ;    - Initialise EAX pour la suite.
            ;    - On copie l'attribut du caract�re de DH � AH afin de colorer le fond.
            ;    - On d�cale EAX de 16 bits vers la gauche afin d'avoir la m�me valeur dans la
            ;      partie haute et basse de EAX.
            ;    - On copie l'attribut du caract�re de DH � AH afin de colorer le fond.
            ;    - Copie du registre EAX dans le terminal � la position du curseur.
            ;    - Ajoute 4 � la position du curseur.
            ;    - Copie du registre EAX dans le terminal � la position du curseur.
            ;    - Ajoute 4 � la position du curseur.
            ; - Sauvegarde du texte :
            ;    - Copie un caract�re de tabulation � la position du registre ECX afin de
            ;      sauvegarder le caract�re pour un �ventuel d�filement, haut ou bas, du terminal.
            ;    - Nous incr�mentons, ensuite, la position du curseur de sauvegarde.
            ; - Sauvegarde des variables :
            ;    - Sauvegarde la position du curseur situ� dans le registre EBX.
            ;    - Sauvegarde la position du curseur de sauvegarde situ� dans le registre ECX.
            ; - Retour :
            ;    - Retourne � la fonction qui v�rifie le caract�re suivant.
            ;---------------------------------------------------------------------------------------
            terminal.afficherTexte.TAB :
			
                ;-----------------------------------------------------------------------------------
                ; Chargement des variables
                ;-----------------------------------------------------------------------------------

                chargerCurseur    ( EBX ) ; Charge la position du curseur dans le registre EBX.
;                chargerSauvegarde ( ECX ) ; Charde la position du curseur de sauvegarde dans ECX.
;                chargerAttribut   ( DL )  ; Charge l'attribut du caract�re dans le registre DL.

                ;-----------------------------------------------------------------------------------
                ; Affichage
                ;-----------------------------------------------------------------------------------
                
;                xor EAX, EAX           ; Initialisation du registre EAX.
                
;                mov AH, DL             ; Copie la valeur de DL dans AH.
;                shl EAX, 16            ; D�calage par 16 bits vers la gauche du registre EAX.
;                mov AH, DL             ; Copie la valeur de DL dans AH.
                
;                mov dword [ EBX ], EAX ; Copie le double mot EAX � la position du curseur.
;                add EBX, 4             ; Ajoute 4 � la position du curseur.
 
;                mov dword [ EBX ], EAX ; Copie le double mot EAX � la position du curseur.
                add EBX, 8             ; Ajoute 4 � la position du curseur.
                
                ;-----------------------------------------------------------------------------------
                ; Sauvegarde du texte
                ;-----------------------------------------------------------------------------------
                                
;                mov byte [ ECX ], TAB ; Ajoute une tabulation � la sauvegarde.
;                inc ECX               ; Incr�mente la position du curseur de savegarde.

                ;-----------------------------------------------------------------------------------
                ; Sauvegarde des variables
                ;-----------------------------------------------------------------------------------

                sauvegarderCurseur    ( EBX ) ; Sauvegarde la position du curseur.
;                sauvegarderSauvegarde ( ECX ) ; Sauvegarde la position du curseur de sauvegarde.
                
                ;-----------------------------------------------------------------------------------
                ; Retour
                ;-----------------------------------------------------------------------------------

                jmp terminal.afficherTexte.verifications ; V�rifie le caract�re suivant.
			
            ;---------------------------------------------------------------------------------------
            ; Cette fonction affiche un saut � la ligne dans le terminal. Pour se faire, elle 
            ; �ffectue les opp�rations suivantes :
            ; - Chargement des variables :
            ;    - Charger la position du curseur dans le registre EAX afin d'obtenir la position 
            ;      d'�criture du caract�re suivant dans le terminal.
            ;    - Charger la position du curseur de sauvegarde dans le registre ECX afin de 
            ;      pouvoir sauvegarder une copie des �l�ments �crits dans le terminal pour un 
            ;      d�filement haut ou bas, �ventuel, du terminal.
            ; - Affichage :
            ;    - Initialisation du registre EDX.
            ;    - Soustrait 0x000B8000 du registre EAX afin d'obtenir seulement le d�placement
            ;      depui le d�but du terminal.
            ;    - Copie la largeur de l'�cran dans le registre EBX.
            ;    - Divise la position du curseur par la largeur de l'�cran afin de savoir sur
            ;      quelle ligne est positionner le curseur.
            ;    - Incr�mente la position du curseur sur ligne suivante.
            ;    - Multiplie la position du curseur par la largeur de l'�cran afin de savoir quel
            ;      est le d�placement depui le d�but du terminal.
            ; - Sauvegarde du texte :
            ;    - Copie un caract�re de retour � la ligne � la position du registre ECX afin de
            ;      sauvegarder le caract�re pour un �ventuel d�filement, haut ou bas, du terminal.
            ;    - Nous incr�mentons, ensuite, la position du curseur de sauvegarde.
            ; - Sauvegarde des variables :
            ;    - Sauvegarde la position du curseur situ� dans le registre EAX.
            ;    - Sauvegarde la position du curseur de sauvegarde situ� dans le registre ECX.
            ; - Retour :
            ;    - Retourne � la fonction qui v�rifie le caract�re suivant.
            ;---------------------------------------------------------------------------------------
            terminal.afficherTexte.CR :
			
                ;-----------------------------------------------------------------------------------
                ; Chargement des variables
                ;-----------------------------------------------------------------------------------

                chargerCurseur    ( EAX ) ; Charge la position du curseur dans EAX.
;                chargerSauvegarde ( ECX ) ; Charde la position du curseur de sauvegarde dans ECX.

                ;-----------------------------------------------------------------------------------
                ; Affichage
                ;-----------------------------------------------------------------------------------

                xor EDX, EDX           ; Initialisation du registre EDX.

                sub EAX, 0x000B8000    ; Soustrait 0x000B8000 au curseur.

                mov EBX, LARGEUR_ECRAN ; Copie la largeur de l'�cran dans le registre EBX.
                
                div EBX                ; Divise la position du curseur par la largeur de l'�cran.
                inc EAX                ; Incr�mente la position du curseur sur ligne suivante.
                mul EBX                ; Multiplie la position du curseur par la largeur de l'�cran.
                
                add EAX, 0x000B8000    ; Ajoute 0x000B8000 au curseur.

                ;-----------------------------------------------------------------------------------
                ; Sauvegarde du texte
                ;-----------------------------------------------------------------------------------

;                mov byte [ ECX ], LF ; Ajoute un retour � la ligne � la sauvegarde.
;                inc ECX              ; Incr�mente la position du curseur de savegarde.

                ;-----------------------------------------------------------------------------------
                ; Sauvegarde des variables
                ;-----------------------------------------------------------------------------------

                sauvegarderCurseur    ( EAX ) ; Sauvegarde la position du curseur.
;                sauvegarderSauvegarde ( ECX ) ; Sauvegarde la position du curseur de sauvegarde.

                ;-----------------------------------------------------------------------------------
                ; Retour
                ;-----------------------------------------------------------------------------------

                jmp terminal.afficherTexte.verifications ; V�rifie le caract�re suivant.

            ;---------------------------------------------------------------------------------------
            ; Cette fonction affiche une tabulation vertical dans le terminal. Pour se faire, elle 
            ; �ffectue les opp�rations suivantes :
            ; - Chargement des variables :
            ;    - Charger la position du curseur dans le registre EAX afin d'obtenir la position 
            ;      d'�criture du caract�re suivant dans le terminal.
            ;    - Charger la position du curseur de sauvegarde dans le registre ECX afin de 
            ;      pouvoir sauvegarder une copie des �l�ments �crits dans le terminal pour un 
            ;      d�filement haut ou bas, �ventuel, du terminal.
            ; - Affichage :
            ;    - Ajoute la valeur 0x9E au registre EBX afin de positionner le curseur sur la
            ;      ligne suivante.
            ; - Sauvegarde du texte :
            ;    - Copie un caract�re de tabulation vertical � la position du registre ECX afin de
            ;      sauvegarder le caract�re pour un �ventuel d�filement, haut ou bas, du terminal.
            ;    - Nous incr�mentons, ensuite, la position du curseur de sauvegarde.
            ; - Sauvegarde des variables :
            ;    - Sauvegarde la position du curseur situ� dans le registre EBX.
            ;    - Sauvegarde la position du curseur de sauvegarde situ� dans le registre ECX.
            ; - Retour :
            ;    - Retourne � la fonction qui v�rifie le caract�re suivant.
            ;---------------------------------------------------------------------------------------
            terminal.afficherTexte.VT :
			
                ;-----------------------------------------------------------------------------------
                ; Chargement des variables
                ;-----------------------------------------------------------------------------------

                chargerCurseur    ( EAX ) ; Charge la position du curseur dans EAX.
;                chargerSauvegarde ( ECX ) ; Charde la position du curseur de sauvegarde dans ECX.

                ;-----------------------------------------------------------------------------------
                ; Affichage
                ;-----------------------------------------------------------------------------------

                add EAX, 0x9E ; Ajoute la valeur 0x9E au registre EAX.
                
                ;-----------------------------------------------------------------------------------
                ; Sauvegarde du texte
                ;-----------------------------------------------------------------------------------

;                mov byte [ ECX ], VT ; Ajoute une tabulation vertical � la sauvegarde.
;                inc ECX              ; Incr�mente la position du curseur de savegarde.

                ;-----------------------------------------------------------------------------------
                ; Sauvegarde des variables
                ;-----------------------------------------------------------------------------------

                sauvegarderCurseur    ( EAX ) ; Sauvegarde la position du curseur.
;                sauvegarderSauvegarde ( ECX ) ; Sauvegarde la position du curseur de sauvegarde.

                ;-----------------------------------------------------------------------------------
                ; Retour
                ;-----------------------------------------------------------------------------------

                jmp terminal.afficherTexte.verifications ; V�rifie le caract�re suivant.

            ;---------------------------------------------------------------------------------------
            ; Cette fonction affiche un retour du chariot dans le terminal. Pour se faire, elle 
            ; �ffectue les opp�rations suivantes :
            ; - Chargement des variables :
            ;    - Charger la position du curseur dans le registre EAX afin d'obtenir la position 
            ;      d'�criture du caract�re suivant dans le terminal.
            ;    - Charger la position du curseur de sauvegarde dans le registre ECX afin de 
            ;      pouvoir sauvegarder une copie des �l�ments �crits dans le terminal pour un 
            ;      d�filement haut ou bas, �ventuel, du terminal.
            ; - Affichage :
            ;    - Initialisation du registre EDX.
            ;    - Soustrait 0x000B8000 du registre EAX afin d'obtenir seulement le d�placement
            ;      depui le d�but du terminal.
            ;    - Copie la largeur de l'�cran dans le registre EBX.
            ;    - Divise la position du curseur par la largeur de l'�cran afin de savoir sur
            ;      quelle ligne est positionner le curseur.
            ;    - Multiplie la position du curseur par la largeur de l'�cran afin de savoir quel
            ;      est le d�placement depui le d�but du terminal.
            ; - Sauvegarde du texte :
            ;    - Copie un caract�re de retour du chariot � la position du registre ECX afin de
            ;      sauvegarder le caract�re pour un �ventuel d�filement, haut ou bas, du terminal.
            ;    - Nous incr�mentons, ensuite, la position du curseur de sauvegarde.
            ; - Sauvegarde des variables :
            ;    - Sauvegarde la position du curseur situ� dans le registre EAX.
            ;    - Sauvegarde la position du curseur de sauvegarde situ� dans le registre ECX.
            ; - Retour :
            ;    - Retourne � la fonction qui v�rifie le caract�re suivant.
            ;---------------------------------------------------------------------------------------
            terminal.afficherTexte.LF :
                ;-----------------------------------------------------------------------------------
                ; Chargement des variables
                ;-----------------------------------------------------------------------------------

                chargerCurseur    ( EAX ) ; Charge la position du curseur dans EAX.
;                chargerSauvegarde ( ECX ) ; Charde la position du curseur de sauvegarde dans ECX.

                ;-----------------------------------------------------------------------------------
                ; Affichage
                ;-----------------------------------------------------------------------------------

                xor EDX, EDX           ; Initialisation du registre EDX.

                sub EAX, 0x000B8000    ; Soustrait 0x000B8000 au curseur.

                mov EBX, LARGEUR_ECRAN ; Copie la largeur de l'�cran dans le registre EBX.
                
                div EBX                ; Divise la position du curseur par la largeur de l'�cran.
                mul EBX                ; Multiplie la position du curseur par la largeur de l'�cran.
                
                add EAX, 0x000B8000    ; Ajoute 0x000B8000 au curseur.

                ;-----------------------------------------------------------------------------------
                ; Sauvegarde du texte
                ;-----------------------------------------------------------------------------------

;                mov byte [ ECX ], CR ; Ajoute un retour � la ligne � la sauvegarde.
;                inc ECX              ; Incr�mente la position du curseur de savegarde.

                ;-----------------------------------------------------------------------------------
                ; Sauvegarde des variables
                ;-----------------------------------------------------------------------------------

                sauvegarderCurseur    ( EAX ) ; Sauvegarde la position du curseur.
;                sauvegarderSauvegarde ( ECX ) ; Sauvegarde la position du curseur de sauvegarde.

                ;-----------------------------------------------------------------------------------
                ; Retour
                ;-----------------------------------------------------------------------------------

                jmp terminal.afficherTexte.verifications ; V�rifie le caract�re suivant.

    %endif ; TERMINAL_ASM_AFFICHERTEXTE

	%ifndef TERMINAL_ASM_AFFICHERNOMBRE
	%define TERMINAL_ASM_AFFICHERNOMBRE
		
	hex:
		pushad
	
		cmp ch, 0x00
		je .8bits
		
		cmp ch, 0x01
		je .16bits
		
		cmp ch, 0x02
		je .32bits
		
		.error:
			ret
	
		.8bits:
			mov cl, 8
			mov edx, 0xf0
			jmp .init
			
		.16bits:
			mov cl, 16
			mov edx, 0xf000
			jmp .init
			
		.32bits:
			mov cl, 32
			mov edx, 0xf0000000

		.init:
			mov edi, [terminal.vCurseur]
			mov ebx, eax

			mov [edi], byte '0'
			add edi, 2
			mov [edi], byte 'x'
			add edi, 2

		.calcul:
			mov eax, ebx		
			sub cl, 4
		
			and eax, edx
			shr eax, cl

			shr edx, 4
		
			cmp al, 9
			jbe .0to9
		
		.atof:
			add al, 55
			jmp .show
		
		.0to9:
			add al, 48
		
		.show:
			mov [edi], byte al
			add edi, 2
		
		.verr:
			cmp cl, 0x00
			ja .calcul
		
		.end:
			mov [terminal.vCurseur], edi
			
			popad
			ret


	%endif ; TERMINAL_ASM_AFFICHERNOMBRE















	%ifndef TERMINAL_ASM_VIDERECRAN
	%define TERMINAL_ASM_VIDERECRAN
;		%ifndef _NASM_MACRO_
;			
;			ax = "value"
;				ah = "Couleur"
;				al = "texte ( 0x00 )"
;			
;			ebx = "position"
;			ecx = "largeur"
;			edx = "hauteur"
;
;		%endif

















	; TODO: Vider �cran
		terminal.viderEcran :
			
			terminal.viderEcran.initialisation :
				and EBX, 0x0000FFFF
				and ECX, 0x0000FFFF
				;chargerAttribut ( AH )
				
				add EBX, BASE_ECRAN
								
			terminal.viderEcran.verifications :
				cmp DL, 0x00
				je terminal.viderEcran.effacementComplet
			
				cmp DL, 0x01
				je terminal.viderEcran.effacerCouleur
				
				cmp DL, 0x02
				je terminal.viderEcran.effacerTexte
				
;				AfficherTexte ( terminal.viderEcran.erreur )
				ret
				
			terminal.viderEcran.effacementComplet :
				mov [ EBX ], word AX
				
				jmp terminal.viderEcran.incrementer
			
			terminal.viderEcran.effacerCouleur :
				mov [ EBX + 1 ], byte AH
				
				jmp terminal.viderEcran.incrementer

			terminal.viderEcran.effacerTexte :
				mov [ EBX ], byte 0x00
			
				jmp terminal.viderEcran.incrementer

			terminal.viderEcran.incrementer :
			
				add EBX, byte 0x02

				cmp ECX, 0x00
				dec ECX
				jg terminal.viderEcran.verifications

				;-----------------------------------------------------------------------------------
				; Retour
				;-----------------------------------------------------------------------------------

				ret ; Retourne � la fonction appelante.
	%endif ; TERMINAL_ASM_VIDERECRAN


    %ifndef TERMINAL_ASM_AFFICHERCURSEUR
    %define TERMINAL_ASM_AFFICHERCURSEUR
        ;-------------------------------------------------------------------------------------------
		; MARK: Afficher curseur vga
        ; Cette fonction affiche le curseur vga dans le terminal. Pour se faire, elle �ffectue les
        ; opp�rations suivantes :
        ; - Chargement des variables :
        ;    - Charge la position du curseur dans le registre EAX afin de pouvoir l'envoyer au
        ;      controleur vga.
        ; - Calcules :
        ;    - �ffectue une mise � z�ro du registre EDX afin qu'il soit utiliser plus tard.
        ;    - Attribut la valeur 2 au registre EBX afin de servir de diviseur.
        ;    - Soustrait la valeur 0x000B8000 au registre EAX afin d'obtenir seulement le
        ;      d�placement du curseur dans le terminal.
        ;    - Divise par BX la valeur des registres AX et DX afin d'obtenir le nombre de
        ;      caract�re pr�c�dant la position du curseur.
        ;    - Copie de AX dans BX pour la suite des instructions.
        ; - Affichage :
        ;    - Envoie des informations d'affichage au controleur vga selon la charte suivante :
        ;      +----------------+--------------------+---------------------------------------------+
        ;      | Num�ro de port | Attribut demander  | Description                                 |
        ;      +----------------+--------------------+---------------------------------------------+
        ;      |     0x03D4     | Num�ro d'index     | Envoie le num�ro de registre � l'index afin |
        ;      |                |                    | de positionner le curseur sur celui-ci.     |
        ;      +----------------+--------------------+---------------------------------------------+
        ;      |     0x03D5     | valeur du registre | Envoie la valeur au registre                |
        ;      +----------------+--------------------+---------------------------------------------+
        ;    - Or, les num�ros d'index afin d'acc�der aux registres du curseur sont 0x0E et 0x0F.
        ;    - Ensuite, les valeurs � envoyer � ces registres sont :
        ;       - 0x0E : l'octet de poid faible de la position du curseur.
        ;       - 0x0F : l'octet de poid fort de cette m�me position.
        ; - Retour :
        ;    - Retourne � la fonction appelante.
        ;-------------------------------------------------------------------------------------------
        terminal.afficherCurseur :
            ;---------------------------------------------------------------------------------------
            ; Chargement des variables
            ;---------------------------------------------------------------------------------------

            chargerCurseur ( EAX ) ; Charge la position du curseur dans le registre EAX.

            ;---------------------------------------------------------------------------------------
            ; Calcules
            ;---------------------------------------------------------------------------------------

            xor EDX, EDX        ; �ffectue une mise � z�ro du registre EDX.
            mov EBX, 2          ; Attribut la valeur 2 au registre EBX.

            sub EAX, 0x000B8000 ; Soustrait la valeur 0x000B8000 au registre EAX.
            div BX              ; Divise les registres AX et DX par BX.

            mov BX, AX          ; Copie la valeur de AX dans BX.

            ;---------------------------------------------------------------------------------------
            ; Affichage
            ;---------------------------------------------------------------------------------------
            
            mov DX, 0x03D4 ; Affecte la valeur 0x03D4 au registre DX.
            mov AL, 0x0E   ; Affecte la valeur 0x0E au registre AL.
            out DX, AL     ; Envoie de AL au port de sortie DX.

            mov DX, 0x03D5 ; Affecte la valeur 0x03D5 au registre DX.
            mov AL, BH     ; Affecte le registre BH au registre AL.
            out DX, AL     ; Envoie de AL au port de sortie DX.
                                                      
            mov DX, 0x03D4 ; Affecte la valeur 0x03D4 au registre DX.
            mov AL, 0x0F   ; Affecte la valeur 0x0F au registre AL.
            out DX, AL     ; Envoie de AL au port de sortie DX.
                                                      
            mov DX, 0x03D5 ; Affecte la valeur 0x03D5 au registre DX.
            mov AL, BL     ; Affecte le registre BL au registre AL.
            out DX, AL     ; Envoie de AL au port de sortie DX.

            ;---------------------------------------------------------------------------------------
            ; Retour
            ;---------------------------------------------------------------------------------------

            ret ; Retourne � la fonction appelante.

    %endif ; TERMINAL_ASM_AFFICHERCURSEUR

%endif ; TERMINAL_ASM
    