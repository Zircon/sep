%include "src/core/debug/term.asm"

%define checksome - (0x1BADB002)

MULTIBOOT_HEADER:
	.magic: dd 0x1BADB002
	.flags: dd 0x00000000
	.checksome: dd checksome
