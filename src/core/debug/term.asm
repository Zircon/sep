%ifndef __DEBUG_TERM_ASM__
%define __DEBUG_TERM_ASM__
        
    %include "inc/core/debug/term.inc"

	__printh__:
		.checking:
			xor edx, edx
			
			shl ecx, 0x00000003
			
			mov esi, [cursor]
			mov ebx, 0x0000000f
			sub ecx, 0x00000004
			shl ebx, cl
			add ecx, 0x00000004
			
			mov [.var], eax
		
		;-------------------------------------------------------------------------------------------
		
		.calcul:
			mov eax, [.var]
			
			sub ecx, 0x00000004

			and eax, ebx
			shr eax, cl
			shr ebx, 0x00000004
			
			cmp eax, 9
			jbe .calcul.2
		
		;-------------------------------------------------------------------------------------------
		
		.calcul.1:
			add al, ('a'-10)
			jmp .printing
		
		;-------------------------------------------------------------------------------------------
		
		.calcul.2:
			add al, '0'
		
		;-------------------------------------------------------------------------------------------
		
		.printing:
			mov [esi], al
			add esi, 0x00000002
			
			cmp ecx, 0x00000000
			ja .calcul

			add esi, 0x00000002

			mov [cursor], esi
			
			jmp __ret__
		
		;-------------------------------------------------------------------------------------------
		
		.var: dq 0x00000000
		
	
	;===============================================================================================
	
    __printf__:
        .checking:
            ; Initialization
            mov al, byte [ esi ]
            inc esi

            ; Checking
            cmp al, null
            je near __ret__

            cmp al, tab
            je .tab

            cmp al, lf
            je .lf

            cmp al, vt
            je .vt

            cmp al, cr
            je .CR

        ;-------------------------------------------------------------------------------------------
        
        .char:
            mov ebx, [cursor]
            mov ah,  [attr]

            mov word [ebx], AX
            add ebx, 0x02
            
            mov [cursor], ebx
            
            jmp .checking

        ;-------------------------------------------------------------------------------------------

        .tab:
            add [cursor], dword 0x00000008
            
            jmp .checking

        ;-------------------------------------------------------------------------------------------

        .lf:
			xor edx, edx
            mov eax, [cursor]

            sub eax, 0x000b8000
            
            mov ebx, 0x000000a0
            
            div bx
            mul bx
            
            add eax, 0x000b8000
            
            mov [cursor], eax

            jmp .checking

        ;-------------------------------------------------------------------------------------------

        .vt:
            add [cursor], dword 0x000000A0
            
            jmp .checking

        ;-------------------------------------------------------------------------------------------

        .CR:
			xor edx, edx
            mov eax, [cursor]
            
            sub eax, 0x000b8000
            
            mov ebx, 0x000000a0
            
            div ebx
            inc eax
            mul ebx
            
            add eax, 0x000b8000
            
            mov [cursor], eax
            
            jmp .checking

    
	;===============================================================================================

	__ret__:
		mov ebx, [cursor]
		
		xor edx, edx
		
		sub ebx, 0x000b8000
		shr bx, 1
		
		mov dx, 0x03D4 ; Affecte la valeur 0x03D4 au registre dx.
		mov al, 0x0E   ; Affecte la valeur 0x0E au registre al.
		out dx, al     ; Envoie de al au port de sortie dx.
			
		mov dx, 0x03D5 ; Affecte la valeur 0x03D5 au registre dx.
		mov al, bh     ; Affecte le registre BH au registre al.
		out dx, al     ; Envoie de al au port de sortie dx.
												  
		mov dx, 0x03D4 ; Affecte la valeur 0x03D4 au registre dx.
		mov al, 0x0F   ; Affecte la valeur 0x0F au registre al.
		out dx, al     ; Envoie de al au port de sortie dx.
												  
		mov dx, 0x03D5 ; Affecte la valeur 0x03D5 au registre dx.
		mov al, bl     ; Affecte le registre BL au registre al.
		out dx, al     ; Envoie de al au port de sortie dx.

		ret
            
    ;===============================================================================================

%endif ; __DEBUG_TERM_ASM__
