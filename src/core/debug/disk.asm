%ifndef __DEBUG_DISK_ASM__
%define __DEBUG_DISK_ASM__

%include "inc/core/debug/disk.inc"
%include "src/core/debug/term.asm"

%define read24 0x08
%define read48 0x09
%define write24 0x0c
%define write48 0x0d

ide_master:   db "The IDE master controler is : ", 0x00
ide_slave:    db "The IDE slave controler is : ", 0x00
drive_master: db "	The master drive is : ", 0x00
drive_slave:  db "	The slave drive is : ", 0x00
present:      db "present", cr, 0x00
not_present:  db "not present", cr, 0x00

request_error: db "Error : invalid request.", cr, 0x00

complete:  db cr, "The disk access is finish.", cr, 0x00

data_struct:
	.rw: db 0x01
	.driver: db 0x00
	.sectcount: dw 0x0001
	.destsource: dd 0x000b8000
	.offset1: dd 0x00000000
	.offset2: dd 0x00000000

list.pos: db 0x00

[SECTION .bss]

list: resb 256

[SECTION .text]
	init:
		.check_list:
			;...
		
		.read_request:
			mov ebx, data_struct ; test only
			
			mov al, [ebx]
			
			cmp al, 0x01
			jz .read
			
			cmp al, 0x02
			jz .write
			
			printf(request_error)
			ret
			
		.read:
			
			mov dx, 0x1f1
			xor eax, eax
			out dx, ax

			mov dx, 0x1f2
			mov ax, [ebx + 2]
			xchg al, ah
			out dx, ax
		
			mov dx, 0x1f3
			mov ah, [ebx + 8]
			mov al, [ebx + 12]			
			out dx, ax
		
			mov dx, 0x1f4
			mov ah, [ebx + 9]
			mov al, [ebx + 13]			
			out dx, al
		
			mov dx, 0x1f5
			mov ah, [ebx + 10]
			mov al, [ebx + 14]			
			out dx, al
		
			mov dx, 0x1f6
			mov al, 0xe0
			out dx, al
		
			
			mov dx, 0x1f7
			mov al, 0x20
			out dx, al

			
			
			ret
			
		.write:
			
			
			ret
			
	__disk_init__:
		xor eax, eax
		xor ebx, ebx
		xor ecx, ecx

		mov bx, 0x00
		mov cl, bl
		
		shl ecx, 1
		add ecx, __DISK_IDE__
		mov dx, 0x01f0

		xor eax, eax

		inc dx
		out dx, al

		inc dx
		mov al, 0x01
		out dx, al

		inc dx
		mov al, 0
		mov ah, 0
		out dx, al

		inc dx
		mov al, 0
		mov ah, 0
		out dx, al

		inc dx
		mov al, 0
		mov ah, 0
		out dx, al

		inc dx
		mov al, 0xe0
		;shl al, 4
		out dx, al
		
		inc dx
		mov al, 0x20
		out dx, al
		
		ret
		
	__disk_read__:
		call __disk_init__
		
		mov dx, 0x01f0
		mov ecx, 0xb8000
		mov ebx, 0xb8600
		;shl ebx, 0x09
	;	add ebx, 0xb8000
		
		.loop:
			in al, dx
			mov [ecx], al
			inc ecx
			
			cmp ecx, ebx
			jbe .loop
			
			printf(complete)
			
		ret
	
	__disk_write__:
	
	__ide_check__:
		mov dx, 0x01f6
		mov al, 0xa0
		out dx, al
		
		mov dx, 0x01f2
		mov ax, 0x0000
		out dx, ax
		
		inc dx
		out dx, ax
		
		inc dx
		out dx, ax
		
		inc dx
		out dx, ax
		
		mov dx, 0x01f7
		mov al, 0xec
		out dx, al
		
		mov dx, 0x01f0
		.read:
			in al, dx
			mov [ebx], al
			inc ebx
			inc ecx
			
			cmp ecx, 0x0100
			jb .read
	
	ret
	
	__disk_check__:
		.check_ide_controller:
			xor eax, eax
			xor ebx, ebx
			xor ecx, ecx
			xor edx, edx
			
			.check_ide_controller.master:
				mov al, 0x87
				mov dx, 0x01f3
				out dx, al
			
				in al, dx

				printf(ide_master)

				cmp al, 0x87
				jne .check_ide_controller.master_not_present
				
				printf(present)
				mov ecx, 0x00000001
				call .check_ide_drive
				jmp .check_ide_controller.slave
				
			.check_ide_controller.master_not_present:	
				printf(not_present)
						
			.check_ide_controller.slave:

				mov al, 0x87
				mov dx, 0x0173
				out dx, al
			
				in al, dx
			
				printf(ide_slave)

				cmp al, 0x87
				jne .check_ide_controller.slave_not_present
				
				printf(present)
				mov ecx, 0x00000001
				call .check_ide_drive
				jmp .check_ide_controller.end
				
			.check_ide_controller.slave_not_present:
				printf(not_present)
				
			.check_ide_controller.end:
				ret

		.check_ide_drive:
			shl ecx, 1
			add ecx, __DISK_IDE__
			mov dx, [ecx]
			mov bx, dx
			
			.check_ide_drive.master:
				mov al, 0xa0
				printf(drive_master)
				call .check_ide_drive.check
				
			.check_ide_drive.slave:
				mov al, 0xb0
				printf(drive_slave)
				mov dx, bx
				
			.check_ide_drive.check:
				add dx, 0x0006
				out dx, al
				
				inc dx
				in al, dx
					
				and al, 0x40
				cmp al, 0x40
				jne .check_ide_drive.not_present
				
				printf(present)
				jmp .check_ide_drive.end
				
			.check_ide_drive.not_present:
				printf(not_present)
			
			.check_ide_drive.end:
				ret
		
		.end:
			ret
%endif ; __DEBUG_DISK_ASM__
