%ifndef __DEBUG_FFS_ASM__
%define __DEBUG_FFS_ASM__
	
	%include "src/core/debug/term.asm"
	
	__ERROR_NOT_FFS__: db 'This is not an ffs file system.', cr, null
	
	__FS_NAME__: db 'File system name: ', null
	__FS_CREATOR__: db 'File system creator: ', null
	
	__fs_loading__:
		.checking:
			mov ebx, edx
			
			cmp dword [ebx], dword 'FFS1'
			jne .not_ffs
			
			add ebx, 0x04
			
			printf(__FS_NAME__)
			printf(ebx)
			add ebx, 0x100
			
			printf(__FS_CREATOR__)
			printf(ebx)
			add ebx, 0x80
			
			ret

		.not_ffs:
			printf(__ERROR_NOT_FFS__)
			ret

%endif ; __DEBUG_FFS_ASM__