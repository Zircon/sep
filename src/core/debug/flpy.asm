%ifndef __DEBUG_FLPY_ASM__
%define __DEBUG_FLPY_ASM__
	
	%include "inc/core/debug/flpy.inc"
	
	__DR1__: db 'Floppy disk 1 is present.', cr, null
	__DR2__: db 'Floppy disk 2 is present.', cr, null
	__DR3__: db 'Floppy disk 3 is present.', cr, null
	__DR4__: db 'Floppy disk 4 is present.', cr, null
	__DR0__: db 'No floppy disk present.', cr, null
	
	__flpy_init__:
	
	__flpy_read__:
	
	__flpy_write__:
	
	__flpy_check__:
		.drive_check:
			xor eax, eax
			mov edx, (__FDC0__+__FLPY_MSR__)
			
			in al, dx
			and al, 0x0f
			mov bl, al
			and al, 0x01
			cmp al, 0x01
			je .drive1
			jmp .end
			
		.drive1:
			printf(__DR1__)
			
			cmp al, 0x02
			je .drive2
			jmp .end
			
		.drive2:
			printf(__DR2__)
			
			cmp al, 0x04
			je .drive3
			jmp .end
			
		.drive3:
			printf(__DR3__)
			
			cmp al, 0x08
			je .drive4
			jmp .end
			
		.drive4:
			printf(__DR4__)
			
		.end:
			printf(__DR0__)
			ret

%endif ; __DEBUG_FLPY_ASM__