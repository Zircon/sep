%ifndef __DEBUG_VIDEO_ASM__
%define __DEBUG_VIDEO_ASM__
    
    __debug.video_info__:
        .type:     db 0x00 ; Text mode.
        .weight:   db 0x50
        .height:   db 0x19
        .charSize: db 0x02
        .ofset:    dd 0x000B8000
    
%endif ; __DEBUG_VIDEO_ASM__