__BLOCK_TYPE__: db 0x01
__DESC_MASK__:  db 0x80
__DESC_NEXT__:  dd 0x00000000, 0x00000000  
times 10-($-$$) db 0x00

    __DESC1_TYPE__:                     db 0x06
    __DESC1_NAME__:                     db 'boot', 0x00
    times 256-($-__DESC1_NAME__)        db 0x00
    __DESC1_AUTHORITY__:                db 0x01
    __DESC1_PROPRIETARY__:              db 'Fos Boot', 0x00
    times 128-($-__DESC1_PROPRIETARY__) db 0x00
    __DESC1_N_FILE__:                   dd 0x00000000
    __DESC1_N_FOLDER__:                 dd 0x00000000
    __DESC1_CREATION_DATE__:            db 0x0A, 0x0A, 0x02, 0x0F, 0x1D
    __DESC1_CONTENT_BLOCK__:            dd 0x00000002, 0x00000000
    __DESC1_ICON__:                     db 'default.png', 0x00
    times 96-($-__DESC1_ICON__)         db 0x00

times 4096-($-$$) db 0x00
