[SECTION FFS.BOOT]
; Boot
    incbin "bin/boot.bin"
    
    times 3072-($-$$) db 0x00

[SECTION FFS.FS_INFO]
; File System Info
    __FS_TYPE__:                 db 'FFS1'
    __FS_NAME__:                 db 'Fos Boot System', 0x00
    times 256-($-__FS_NAME__)    db 0x00
    __FS_CREATOR__:              db 'Fos Boot', 0x00
    times 128-($-__FS_CREATOR__) db 0x00
    __FS_VERSION__:              dw 0x0000
    __FS_SUBTYPE__:              dw 0x0003
    __FS_N_BLOCK__:              dd 0x00020000, 0x00000000
    __FS_BLOCK_SIZE__:           db 0x08
    __FS_AUTHORITY__:            db 0x03
    __FS_CREATION_DATE__:        db 0x0A, 0x0A, 0x02, 0x0C, 0x11
    __FS_N_FILE__:               dd 0x00000000
    __FS_N_FOLDER__:             dd 0x00000000
    times 512-($-$$)             db 0x00

[SECTION FFS.ROOT]
; Root Info
    __ROOT_AUTHORITY__:       db 0x03
    __ROOT_PROPRIETARY__:     db 'Fos Boot', 0x00
    times 128-($-__ROOT_PROPRIETARY__) db 0x00
    __ROOT_FILE_DESC_BLOCK__: dd 0x00000001, 0x00000000
    __ROOT_N_FILE__:          dd 0x00000000
    __ROOT_N_FOLDER__:        dd 0x00000000
    times 512-($-$$)          db 0x00
    
; Block 1
    incbin "res/block1.bin"
    incbin "res/block2.bin"
    incbin "res/block3.bin"
    