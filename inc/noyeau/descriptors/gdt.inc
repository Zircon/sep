;---------------------------------------------------------------------------------------------------
; File name:        gdt.inc
; Version:          1.0.0.0
; Author:           Maxime Jeanson
; Copyright:        (c) 2010 by Sep System, all rights reserved.
;
; Documentations:   file:///usr/doc/system/kernel/gdt/index.html
;
; Contains:         This file contain the deffinitions of the fonctions in the gdt.asm file.
;
;                   These fonctions are in this file:
;                    - __gdt.init__:
;                    - __gdt.add.desc__:
;---------------------------------------------------------------------------------------------------

%ifndef __GDT_INC__
%define __GDT_INC__
	; MARK: fonction definitions
		%define gdt.init call __gdt.init__
		%define gdt.add.desc( name, pos, offset, limite, attr ) _m_gdt.add.desc_ name, pos, offset, limite, attr
	
	; MARK: macro definitions
		%macro _m_gdt.add.desc_ 5
		
			%define %1 %2*8
			
			mov eax, %3
			mov ebx, %4
			mov cx,  %5
			
			call __gdt.add.desc__
		
		%endmacro
	
%endif ; __GDT_INC__

%include "src/noyeau/descriptors/gdt.asm"