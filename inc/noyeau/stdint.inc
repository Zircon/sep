%define uint.set( dest, src ) mov dest, src
%define uint.get( dest, src ) mov dest, src
%define uint.inc( dest ) inc dest
%define uint.dec( dest ) dec dest
%define uint.add( dest, src ) add dest, src
%define uint.sub( dest, src ) sub dest, src

%macro uint8 1-2
	%ifndef _uint8_._preprocessor_._%1_
		%define _uint8_._preprocessor_._%1_
		
		%define %1.ptr _uint8_._%1_
		%define %1.val byte [ _uint8_._%1_ ]
		%define %1.set( src ) uint.set ( %1.val, src )
		%define %1.get( dest ) uint.set ( dest, %1.val )
		%define %1.add( src ) uint.set ( %1.val, src )

		%if %0 == 1
			_uint8_._%1_ : db 0x00
		%else
			_uint8_._%1_ : db %2
		%endif
	
	%else
		%error 'This variable was defined before !'
	%endif
%endmacro

uint8 var, 0x13
var.get ( byte EAX )