%ifndef TERMINAL_INC
%define TERMINAL_INC

    %ifndef TERMINAL_INC_DEFINITIONS
    %define TERMINAL_INC_DEFINITIONS

        %define NUL 0x00 ; Valeur "Nulle".
        %define SOH 0x01 ; Permet de modifier l'attribut du texte suivant.
        %define TAB 0x09 ; Valeur d'une "Tabulation".
        %define LF  0x0A ; Valeur d'un "Retour � la ligne".
        %define VT  0x0B ; Valeur d'une "Tabulation vertical".
        %define CR  0x0D ; Valeur d'un "Retour du chariot".

		%define BASE_ECRAN    0x000B8000 ; Addresse de base de l'�cran.
        %define LARGEUR_ECRAN 0x000000A0 ; Largeur de l'�cran.
        %define HAUTEUR_ECRAN 0x00000019 ; Hauteur de l'�cran.
		%define LONGEUR_ECRAN 0x00000FA0 ; Longeur de l'�cran.
		%define FIN_ECRAN     0x000B8FA0 ; Addresse de fin de l'�cran.

    %endif ; TERMINAL_INC_DEFINITIONS

    %ifndef TERMINAL_INC_FONCTIONS
    %define TERMINAL_INC_FONCTIONS

		%define EtablirAttribut( attribut )    _m_etablir_attribut_   attribut
		%define AfficherCaractere( caractere ) _m_afficher_caractere_ caractere
		%define AfficherTexte( texte )         _m_afficher_texte_     texte
		%define RemplirEcran( cmd8, dec, lng, color ) _m_vider_ecran_        cmd8, dec, lng, color

		%define setColor( color8 ) _m_etablir_attribut_   color8
		%define printc( char8 )    _m_afficher_caractere_ char8
		%define printf( text32 )   _m_afficher_texte_     text32

	%endif ; TERMINAL_INC_FONCTIONS

    %ifndef TERMINAL_INC_MACROS
    %define TERMINAL_INC_MACROS

		%macro _m_afficher_caractere_ 1

			pushad

            mov AL, %1

            call terminal.afficherCaractere
            
			popad
			
        %endmacro
		
        %macro _m_etablir_attribut_ 1

            mov byte [ terminal.vAttribut ], %1

        %endmacro
		
		%macro _m_afficher_nombre_ 2
			
			pushad
			
			mov bl,  %1
			mov EAX, %2
			
			call terminal.afficherNombre
			
			popad
			
		%endmacro
		
		%macro _m_afficher_texte_ 1

			pushad

            mov ESI, %1

            call terminal.afficherTexte

			popad
			
        %endmacro

		%macro _m_vider_ecran_ 4
		
			%if ( %1 > 0x03 )
				%error La commande est invalide.
			%else
				
				push EAX
				push EBX
				push ECX
				push EDX
					
					xor eax, eax
					
					mov ah, %4
					mov EBX, %2
					mov ECX, %3
					mov DL,  %1
					
					call terminal.viderEcran

				pop EAX
				pop EBX
				pop ECX
				pop EDX

			%endif

		%endmacro

	%endif ; TERMINAL_INC_MACROS
	
%endif ; TERMINAL_INC