;---------------------------------------------------------------------------------------------------
; core/debug/term.inc
;
; Macro and defines of term.asm.
;---------------------------------------------------------------------------------------------------

%ifndef __DEBUG_TERM_INC__
%define __DEBUG_TERM_INC__

    %define null 0x00
    %define tab  0x09
    %define lf   0x0a
    %define vt   0x0b
    %define cr   0x0d

    %define printf(text) printf text
	%define printh(val, pos) printh val, pos

	%define attr(val) mov byte [attr], val

    ;===============================================================================================

    %macro printf 1

		pushad

        mov ESI, %1

        call __printf__

		popad

    %endmacro

	;-----------------------------------------------------------------------------------------------
	
	%macro printh 2
		
		pushad
				
		;mov eax, %1		
		mov ecx, %2
		
		call __printh__
		
		popad
		
	%endmacro
	
    ;===============================================================================================
	
    cursor: dd 0x000B8000 ; Current cursor position.
    attr:   db 0x07       ; Current character attributes.

%endif ; __DEBUG_TERM_INC__
