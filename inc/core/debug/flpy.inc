%ifndef __DEBUG_FLPY_INC__
%define __DEBUG_FLPY_INC__

	%define __FDC0__ 0x03f0
	%define __FDC1__ 0x0370

	%define __FLPY_SRA__  0x00
	%define __FLPY_SRB__  0x01
	%define __FLPY_DOR__  0x02
	%define __FLPY_TDR__  0x03
	%define __FLPY_MSR__  0x04
	%define __FLPY_DSR__  0x04
	%define __FLPY_FIFO__ 0x05
	%define __FLPY_DIR__  0x07
	%define __FLPY_CCR__  0x07
	
%endif ; __DEBUG_FLPY_INC__