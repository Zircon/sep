cd /Volumes/KINGSTON/Documents/Documentations/Sep/

echo Compiling please wait...
echo 

nasm -f bin "src/core/boot/boot.asm"           -o "lib/core/boot/boot.bin"

nasm -f bin "src/chargeur/boot.asm"            -o "bin/boot.bin"

nasm -f bin "res/chargeur/mbr.asm"             -o "res/mbr.bin"
nasm -f bin "res/chargeur/block1.asm"          -o "res/block1.bin"
nasm -f bin "res/chargeur/block2.asm"          -o "res/block2.bin"
nasm -f bin "res/chargeur/block3.asm"          -o "res/block3.bin"
nasm -f bin "res/chargeur/part.asm"            -o "res/part.bin"

nasm -f bin "src/chargeur/chargeur.asm"        -o "bin/chargeur.bin"

nasm -f bin "src/noyeau/terminal/terminal.asm" -o "bin/terminal/terminal.bin"
nasm -f bin "src/noyeau/noyeau.asm"            -o "bin/noyeau.bin"

nasm -f bin "sep.asm"                          -o "sep.img"
